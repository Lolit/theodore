package fr.minesparis.ae.caross.gson;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Louis MEYRAT on 20/01/2017.
 *
 * Contient les données de DetailsDepartActiviry
 *
 * Cette classe est Parcelable, car elle fait partie des attributs de EtatDepart, et il est stocké dans un bundle par NouveauDépartActivity
 *
 * Généré avec http://www.jsonschema2pojo.org/
 */


public class DetailsDepart implements Parcelable
{

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("idSoiree")
    @Expose
    private int idSoiree;
    @SerializedName("lat")
    @Expose
    private float lat;
    @SerializedName("lon")
    @Expose
    private float lon;
    @SerializedName("adresse")
    @Expose
    private String adresse;
    @SerializedName("transport")
    @Expose
    private int transport;
    @SerializedName("heure")
    @Expose
    private Date heure;
    @SerializedName("distance")
    @Expose
    private float distance;
    @SerializedName("placesTotales")
    @Expose
    private int placesTotales;
    @SerializedName("placesUtilisees")
    @Expose
    private int placesUtilisees;
    @SerializedName("nom")
    @Expose
    private String nom;
    @SerializedName("id_auteur")
    @Expose
    private String idAuteur;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("commentaire")
    @Expose
    private String commentaire;
    @SerializedName("rendezVous")
    @Expose
    private String rendezVous;
    public final static Parcelable.Creator<DetailsDepart> CREATOR = new Creator<DetailsDepart>() {


        public DetailsDepart createFromParcel(Parcel in) {
            DetailsDepart instance = new DetailsDepart();
            instance.id = in.readInt();
            instance.idSoiree = in.readInt();
            instance.lat = in.readFloat();
            instance.lon = in.readFloat();
            instance.adresse = in.readString();
            instance.transport = in.readInt();
            instance.heure = new Date(in.readLong());
            instance.distance = in.readFloat();
            instance.placesTotales = in.readInt();
            instance.placesUtilisees = in.readInt();
            instance.idAuteur = in.readString();
            instance.nom = in.readString();
            instance.contact = in.readString();
            instance.commentaire = in.readString();
            instance.rendezVous = in.readString();
            return instance;
        }

        public DetailsDepart[] newArray(int size) {
            return (new DetailsDepart[size]);
        }

    }
            ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdSoiree() {
        return idSoiree;
    }

    public void setIdSoiree(int idSoiree) {
        this.idSoiree = idSoiree;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getTransport() {
        return transport;
    }

    public void setTransport(int transport) {
        this.transport = transport;
    }

    public Date getHeure() {
        return heure;
    }

    public void setHeure(Date heure) {
        this.heure = heure;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public int getPlacesTotales() {
        return placesTotales;
    }

    public void setPlacesTotales(int placesTotales) {
        this.placesTotales = placesTotales;
    }

    public int getPlacesUtilisees() {
        return placesUtilisees;
    }

    public void setPlacesUtilisees(int placesUtilisees) {
        this.placesUtilisees = placesUtilisees;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getIdAuteur() {
        return idAuteur;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public String getRendezVous() {
        return rendezVous;
    }

    public void setRendezVous(String rendezVous) {
        this.rendezVous = rendezVous;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(idSoiree);
        dest.writeFloat(lat);
        dest.writeFloat(lon);
        dest.writeString(adresse);
        dest.writeInt(transport);
        dest.writeLong(heure.getTime());
        dest.writeFloat(distance);
        dest.writeInt(placesTotales);
        dest.writeInt(placesUtilisees);
        dest.writeString(idAuteur);
        dest.writeString(nom);
        dest.writeString(contact);
        dest.writeString(commentaire);
        dest.writeString(rendezVous);
    }

    public int describeContents() {
        return 0;
    }

    //pour contacter l'auteur du départ
    public void envoyerSMSAuteur(Context contx){
        if(contact.length()>8) { //on fait une vérification sommaire du numéro
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.setData(Uri.parse("sms:" + contact.trim()));
            contx.startActivity(sendIntent);
        }
    }

}