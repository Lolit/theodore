package fr.minesparis.ae.caross.http;

import java.util.List;

import fr.minesparis.ae.caross.gson.CodeErreur;
import fr.minesparis.ae.caross.gson.ConnexionReponse;
import fr.minesparis.ae.caross.gson.Depart;
import fr.minesparis.ae.caross.gson.DetailsDepart;
import fr.minesparis.ae.caross.gson.EtatDepart;
import fr.minesparis.ae.caross.gson.RequeteDeparts;
import fr.minesparis.ae.caross.gson.Soiree;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Louis MEYRAT on 15/12/2016.
 *
 * Utiliséz par Retrofit : définie les différentes requêtes http possibles et leur résultats attendus
 */

public interface APIInterface {

    @FormUrlEncoded
    @POST("connexion")
    Call<ConnexionReponse> connexion(@Field("id") String idToken, @Field("email") String email, @Field("nom") String nom, @Field("contact") String contact);

    @GET("deconnexion")
    Call<Void> deconnexion();

    @GET("soirees")
    Call<List<Soiree>> getSoirees();

    @POST("departs")
    Call<List<Depart>> getDeparts(@Body RequeteDeparts requete);

    @POST("departs/nouveau")
    Call<CodeErreur> nouveauDepart(@Query("token") String token, @Query("nouveau") int nouveau,  @Body DetailsDepart nouveau_depart);
    //prend en paramètre le token de FCM, pour que l'on puisse envoyer des messages à l'utilisateur

    @GET("departs/{id}")
    Call<DetailsDepart> getDepartInfo(@Path("id") int id);

    @GET("departencours")
    Call<EtatDepart> getDepartEnCours();

    @FormUrlEncoded
    @POST("departs/{id}/inscrire")
    Call<ConnexionReponse> inscrire(@Path("id") int idDepart, @Field("token") String token);
    //prend en paramètre le token de FCM, pour que l'on puisse envoyer des messages à l'utilisateur

    @FormUrlEncoded
    @POST("departs/{id}/inscrire/{ami}")
    Call<ConnexionReponse> inscrireAmi(@Path("id") int idDepart, @Path("ami") String ami, @Field("token") String token);

    @GET("depart/desinscrire")
    Call<ConnexionReponse> desinscrire();

    @GET("depart/quitter")
    Call<ConnexionReponse> quitterDepart();

    @FormUrlEncoded
    @POST("utilisateur/signaler")
    Call<Void> signaler(@Field("nom") String nom, @Field("motif") String motif);

    @FormUrlEncoded
    @POST("depart/message")
    Call<ConnexionReponse> envoyerMessage(@Field("message") String message);

    @Multipart
    @POST("nouvelle_soiree")
    Call<ConnexionReponse> nouvelleSoiree(
            @Part("nom") RequestBody nom,
            @Part("auteur") RequestBody auteur,
            @Part("adresse") RequestBody adresse,
            @Part("ville") RequestBody ville,
            @Part("code") RequestBody codePostal,
            @Part("debut") RequestBody heureDebut,
            @Part("fin") RequestBody fin,
            @Part("description") RequestBody description,
            @Part("erreurs_application") RequestBody estApplication,
            @Part MultipartBody.Part file
    );
}
