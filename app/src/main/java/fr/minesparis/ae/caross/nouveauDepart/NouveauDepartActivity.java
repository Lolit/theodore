package fr.minesparis.ae.caross.nouveauDepart;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.android.gms.location.places.Place;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Calendar;

import fr.minesparis.ae.caross.EtatDepartActivity;
import fr.minesparis.ae.caross.R;
import fr.minesparis.ae.caross.connexion.ConnexionInfo;
import fr.minesparis.ae.caross.gson.CodeErreur;
import fr.minesparis.ae.caross.gson.DetailsDepart;
import fr.minesparis.ae.caross.http.APIGetter;
import fr.minesparis.ae.caross.http.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.gms.location.places.ui.PlacePicker.getPlace;
import static fr.minesparis.ae.caross.nouveauDepart.FragmentNDepartDestination.PLACE_PICKER_REQUEST;

/**
 * Activity qui gère la création d'un nouveau départ. Contient des Fragment qui se succèdent pour présenter tous les champs
 */
public class NouveauDepartActivity extends AppCompatActivity {

    public static final String DEPART = "DEPART";
    private DetailsDepart depart;
    View toolbar; //pour les SnackBars, il faut une view qui soit toujours dans l'Activity
    private boolean nouveau = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nouveau_depart);

        setSupportActionBar((Toolbar)findViewById(R.id.nouveau_depart_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //si l'activité ne s'est jamais lancée, on affiche le premier Fragment
        if (savedInstanceState == null) {
            showFragment(new FragmentNDepartHeure(), false);
            depart = getIntent().getParcelableExtra(DEPART);
            if(depart==null)
            {
                depart = new DetailsDepart();
                depart.setIdSoiree(getIntent().getIntExtra("idSoiree", 1));
                nouveau = true;
            }else{
                getSupportActionBar().setTitle(R.string.nouveau_depart_modifier);
                nouveau = false;
            }


        }else{
            //on récupère le départ qui était en cours de création
            depart = savedInstanceState.getParcelable("depart");
            nouveau = savedInstanceState.getBoolean("nouveau", true);
        }

        toolbar = findViewById(R.id.nouveau_depart_toolbar);

    }
    public void showFragment(final Fragment fragment, boolean backable) {
        //change le fragment de l'activity. Code trouvé sur https://developers.google.com
        if (fragment == null) {
            return;
        }

        // Begin a fragment transaction.
        final FragmentManager fm = getSupportFragmentManager();
        final FragmentTransaction ft = fm.beginTransaction();
        // We can also animate the changing of fragment.
        // ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        ft.setCustomAnimations(R.anim.entree, R.anim.sortie, R.anim.retour_entree, R.anim.retour_sortie);
        // Replace current fragment by the new one.
        ft.replace(R.id.ndepart_placeholder, fragment, fragment.getClass().getSimpleName());
        // Null on the back stack to return on the previous fragment when user
        // press on back button.
        if (backable)ft.addToBackStack(null);

        // Commit changes.
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        //on ne revient pas tout de suite à l'Activity précédente, mais au fragment précédent
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
    //pour PlacePicker, qui permet d'entrer une adresse
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            FragmentNDepartDestination fragDestination = (FragmentNDepartDestination)
                    getSupportFragmentManager().findFragmentByTag(FragmentNDepartDestination.class.getSimpleName());
            if (fragDestination != null) { //si on est bien resté sur le bon fragment
                if(fragDestination.progres!=null)fragDestination.progres.dismiss(); //on annule la boîte de dialogue
                if (resultCode == RESULT_OK) {
                    Place place = getPlace(this, data);

                    if(place.getAddress()!=null) {
                        fragDestination.setAdresse(place.getAddress());
                        depart.setAdresse(place.getAddress().toString());
                        depart.setLat((float) place.getLatLng().latitude);
                        depart.setLon((float) place.getLatLng().longitude);
                    }else{
                        Snackbar.make(toolbar, R.string.erreur_adresse, Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle save) {
        //si l'activity passe en arrière plan, elle peut être tuée par l'OS : on sauvegarde donc depart, que l'on veut garder
        super.onSaveInstanceState(save);
        save.putParcelable("depart", depart);
        save.putBoolean("nouveau", nouveau);
    }

    public void setHeure(Calendar cal){
        depart.setHeure(cal.getTime());
    }

    public void setCommentaire(String commentaire){
        depart.setCommentaire(commentaire);
    }
    public void setRDV(String rdv){
        depart.setRendezVous(rdv);
    }
    public void setTransport(int transport){
        depart.setTransport(transport);
    }
    public void setPlaces(int totales){
        depart.setPlacesTotales(totales);
        depart.setPlacesUtilisees(0);
    }
    public DetailsDepart getDepart(){
        return depart;
    }

    public void envoyerDepart(){
        APIInterface mApiService = APIGetter.getAPIservice();
        Call<CodeErreur> mService = mApiService.nouveauDepart(FirebaseInstanceId.getInstance().getToken(), nouveau?1:0, depart);
        mService.enqueue(new Callback<CodeErreur>() {
            @Override
            public void onResponse(Call<CodeErreur> call, Response<CodeErreur> response) {
                if (!response.isSuccessful()) {
                    Log.e("Connexion", response.message());
                    Snackbar.make(toolbar, "Erreur: réponse du serveur : "+response.code(), Snackbar.LENGTH_LONG).show();
                    return;
                }
                CodeErreur rep = response.body();//on obtient une instance de CodeErreur
                Log.d("info ", "Reçu code " + response.code());
                if(rep.code==0){
                    startActivity(new Intent(NouveauDepartActivity.this, EtatDepartActivity.class));
                    ConnexionInfo.setDepartEnCours(NouveauDepartActivity.this, depart.getHeure().getTime());
                    finish();
                    //retour à la liste des départs
                }else{
                    DialogErreur.newInstance(rep.code).show(getSupportFragmentManager(), "DialogErreur");
                }

            }

            @Override
            public void onFailure(Call<CodeErreur> call, Throwable t) {
                call.cancel();
                Log.e("Connexion", "Erreur fatale de connexion");
                Snackbar.make(toolbar, R.string.msg_internet_erreur, Snackbar.LENGTH_LONG).show();
            }
        });

    }
    public boolean isNouveau(){
        return nouveau;
    }





}
