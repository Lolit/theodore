package fr.minesparis.ae.caross.connexion;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import fr.minesparis.ae.caross.EtatDepartActivity;
import fr.minesparis.ae.caross.R;
import fr.minesparis.ae.caross.SoireesActivity;
import fr.minesparis.ae.caross.http.APIGetter;
import fr.minesparis.ae.caross.http.APIInterface;
import fr.minesparis.ae.caross.notification.NotificationPublisher;
import retrofit2.Call;
import retrofit2.Callback;

import static android.app.PendingIntent.FLAG_ONE_SHOT;
import static android.content.ContentValues.TAG;
import static fr.minesparis.ae.caross.notification.NotificationPublisher.DEPART_EN_COURS;
import static fr.minesparis.ae.caross.notification.NotificationPublisher.DEPART_IMMINENT;

/**
 * Created by Louis MEYRAT on 22/12/2016.
 *
 * Classe statique qui
 * -stocke les informations de connexion de la session en cours
 * -possède des fonctions statiques auquelles on veut pouvoir accéder partout dans l'appli
 */

public class ConnexionInfo {


    public static String email="";
    public static String nom="";
    public static boolean estConnecte=false;

    //on ne la met pas final, car on veut pouvoir la changer dans les préférences
    public static String BASE_URL = "https://keewit.fr/index.php/";
    public static String phpsessid = "";

    //pour la conversion SQL/Java des dates
    private static final SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
    private static final SimpleDateFormat dfh = new SimpleDateFormat("HH:mm", Locale.US);
    public static final SimpleDateFormat DFH_FULL = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);
    private static PendingIntent alarmIntent; //pour pouvoir annuler l'alarme de départ imminant

    /**
     * Initialise BASE_URL à partir des préférences
     * N'initialise PAS email et mdp, car on veut vérifier qu'ils sont valides avant
     * @param prefs les préférences de l'application
     */
    public static void initialiserPrefs(SharedPreferences prefs){
        if(prefs.getString("base_url", "").equals("theodore.000webhostapp.com")){
            Log.d("Preferences", "Changement de serveur...");
            prefs.edit().putString("base_url", "keewit.fr").apply();
            BASE_URL = "http://keewit.fr/index.php/";
        }else{
            BASE_URL = "http://"+prefs.getString("base_url", "keewit.fr")+"/index.php/";
        }
        Log.d(TAG, "FCM token: " + FirebaseInstanceId.getInstance().getToken());
    }
    public static void setContact(Context cont, String contact, String email){
        cont.getApplicationContext().getSharedPreferences(email, 0).edit().putString("CONTACT", contact).commit();
        Log.d("contact", cont.getApplicationContext().getSharedPreferences(email, 0).getString("CONTACT", null));
    }
    public static String getContact(Context cont, String email){
        return cont.getApplicationContext().getSharedPreferences(email, 0).getString("CONTACT", null);
    }

    public static SimpleDateFormat getDateFormat(){
        return df;
    }
    public static SimpleDateFormat getHeureFormat(){
        return dfh;
    }

    public static void setDepartEnCours(Context contx, long time){
        //si c'est un changement, on enlève les notifs
        long departEnCours = getDepartEnCours(contx);
        if(departEnCours!=0 && departEnCours!= time) {
            notifierDepart(contx, 0);
        }
        //définit qu'un départ est en cours
        SharedPreferences prefs = contx.getSharedPreferences(email, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();

        if(time==0){
            edit.remove("DEPART_TIME");
        }else{
            edit.putLong("DEPART_TIME", time);
        }
        edit.apply();

        notifierDepart(contx, time);
    }
    public static Snackbar afficherDepartEnCours(final AppCompatActivity contx, View v, Snackbar sb){
        //affiche une SnackBar aui montre qu'un départ est en cours. v est une view quelconque, pourvu qu'elle soit affichée par contx
        if(isDepartEnCours(contx)) {
            sb = Snackbar.make(v, contx.getString(R.string.depart_en_cours), Snackbar.LENGTH_INDEFINITE)
                    .setAction("Aller", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(contx, EtatDepartActivity.class);
                            contx.startActivity(intent);
                        }
                    });
            sb.setActionTextColor(Color.YELLOW);
            sb.getView().setBackgroundColor(contx.getResources().getColor(R.color.colorPrimary));
            sb.show();

        }else{
            if(sb!=null && sb.isShown()){
                sb.dismiss();
                sb=null;
            }
        }
        return sb;
    }
    private static void notifierDepart(Context contx, long heure){
        //affiche les notifications de Départ en cours
        if(heure == 0){
            //on annule toutes les notifications
            NotificationManager mNotification = (NotificationManager) contx.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotification.cancel(DEPART_EN_COURS);

            if(alarmIntent!=null) { //si on a lancé une alarme
                AlarmManager alarmManager = (AlarmManager) contx.getSystemService(Context.ALARM_SERVICE);
                alarmManager.cancel(alarmIntent);
            }
            return;
        }

        NotificationManager mNotification = (NotificationManager) contx.getSystemService(Context.NOTIFICATION_SERVICE);

        //création de la notification Départ en cours

        Intent launchNotifiactionIntent = new Intent(contx, EtatDepartActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(contx,
                DEPART_EN_COURS, launchNotifiactionIntent,
                FLAG_ONE_SHOT);

        Notification notif = new NotificationCompat.Builder(contx)
                .setWhen(System.currentTimeMillis())
                .setTicker("Départ en cours")
                .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(BitmapFactory.decodeResource(contx.getResources(),
                        R.mipmap.ic_launcher))
                .setContentTitle("Départ en cours")
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Vous vous êtes inscrit au départ de "+dfh.format(heure)+"\nN'oubliez pas d'y aller !"))
                .setOngoing(true)
                .setContentIntent(pendingIntent)
                .build();
        notif.defaults |= Notification.DEFAULT_ALL;

        mNotification.notify(DEPART_EN_COURS, notif);
        //fin de la création de la notification départ en cours

        //création de la notification départ imminent

        notif = new NotificationCompat.Builder(contx)
                .setWhen(heure-600000)
                .setTicker("Départ imminent !")
                .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(BitmapFactory.decodeResource(contx.getResources(),
                        R.mipmap.ic_launcher))
                .setContentTitle("Départ Imminent")
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Votre départ part très prochainement ! \nCliquez sur cette notification pour revoir le point de rendez-vous et l'heure exatcte du départ"))
                .setOngoing(false)
                .setContentIntent(pendingIntent)
        .build();
        notif.defaults |= Notification.DEFAULT_ALL;

        //on ne l'afiche pas dirrectement, mais on la met dans un Intent, qui sera envoyé à NotificationPublisher 10 min avant le départ
        Intent notificationIntent = new Intent(contx, NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_ID, DEPART_IMMINENT);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notif);
        alarmIntent = PendingIntent.getBroadcast(contx, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager)contx.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, heure - 600000, alarmIntent);
    }

    public static boolean isDepartEnCours(Context contx){
        SharedPreferences prefs = contx.getSharedPreferences(email, Context.MODE_PRIVATE);
        Long time = prefs.getLong("DEPART_TIME", 0);
        return time > Calendar.getInstance().getTimeInMillis();
    }
    public static long getDepartEnCours(Context contx){
        SharedPreferences prefs = contx.getSharedPreferences(email, Context.MODE_PRIVATE);
        return prefs.getLong("DEPART_TIME", 0);
    }


    public static void deconnecter(final AppCompatActivity cont) {

        //déconnecte l'utilisateur de Firebase et du serveur

        ConnexionInfo.estConnecte = false;
        SoireesActivity.erreurConnexion = false;

        setDepartEnCours(cont, 0L);

        AuthUI.getInstance()
                .signOut(cont)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        // l'utilistateur est déconecté
                        cont.startActivity(new Intent(cont, SoireesActivity.class));
                        cont.finish();
                    }
                });
        APIInterface mApiService = APIGetter.getAPIservice();
        Call<Void> mService = mApiService.deconnexion();
        mService.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, retrofit2.Response<Void> response) {
                //il faudrait peut-être mettre quelque chose ici...
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("Deconnexion", "Echec de la déconnexion au serveur");
            }
        });
    }

}
