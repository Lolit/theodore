package fr.minesparis.ae.caross.views;

import android.animation.ObjectAnimator;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fr.minesparis.ae.caross.R;
import fr.minesparis.ae.caross.gson.Soiree;

/**
 * Created by Louis MEYRAT on 14/12/2016.
 *
 * Gère l'affichage de la liste des soirées
 */

public class SoireesAdapter extends RecyclerView.Adapter<SoireeHolder> {

    private static final int ANIMATION_TIME = 500;

    private List<Soiree> soirees;
    private int mExpandedPosition = -1;
    private RecyclerView recyclerView;
    private int baseHeight=0, webHeight = 0;

    //Soirees est la liste des models à afficher
    public SoireesAdapter(List<Soiree> soirees, RecyclerView recyclerView) {
        this.soirees = soirees;
        this.recyclerView=recyclerView;
        webHeight = (int) recyclerView.getResources().getDimension(R.dimen.web_height);
    }


    @Override
    public SoireeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.soiree_elem,parent,false);
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        baseHeight = view.getMeasuredHeight();
        view.getLayoutParams().height=baseHeight;
        return new SoireeHolder(view);
    }

    @Override
    public void onBindViewHolder(final SoireeHolder holder, int position) {
        final boolean isExpanded = holder.getAdapterPosition()==mExpandedPosition;

        holder.details.setVisibility(isExpanded ? View.VISIBLE : View.GONE);

        holder.itemView.setActivated(isExpanded);
        holder.itemView.getLayoutParams().height = isExpanded?baseHeight+webHeight:baseHeight;


        holder.expandButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int prev = isExpanded ?-1:mExpandedPosition;
                Log.d("prev", "= "+prev);
                mExpandedPosition = isExpanded ? -1 : holder.getAdapterPosition();

                ObjectAnimator anim = ObjectAnimator.ofFloat(holder.expandButton, "rotation",holder.expandButton.getRotation(), (holder.expandButton.getRotation() + 180)%360);
                anim.setDuration(ANIMATION_TIME);
                anim.start();

                recyclerView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        SoireeHolder prevV = (SoireeHolder) recyclerView.findViewHolderForLayoutPosition(prev);

                        notifyItemChanged(holder.getAdapterPosition());


                        if(prevV!=null){
                            prevV.expandButton.setRotation(0);
                            notifyItemChanged(prev, null);
                        }



                    }
                }, ANIMATION_TIME);


            }
        });

        holder.bind(soirees.get(position));
        holder.expandButton.setRotation(isExpanded?180:0);

    }
    public void resetDataSet(){
        mExpandedPosition = -1;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return soirees.size();
    }
    public List<Soiree> getSoirees(){
        return soirees;
    }
}

