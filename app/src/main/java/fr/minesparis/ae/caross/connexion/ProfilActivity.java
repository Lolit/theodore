package fr.minesparis.ae.caross.connexion;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import fr.minesparis.ae.caross.R;
import fr.minesparis.ae.caross.SoireesActivity;

/**
 * Affiche les informations de profil de l'utilisateur
 *
 *
 */
public class ProfilActivity extends AppCompatActivity{

    FirebaseAuth auth;
    @Override
   public void onCreate(Bundle bundle)
   {
       super.onCreate(bundle);
       setContentView(R.layout.activity_profil);
       TextView email =  findViewById(R.id.profil_email);
       TextView nom =  findViewById(R.id.profil_nom);
       TextView contact =  findViewById(R.id.profil_contact);

       auth = FirebaseAuth.getInstance();
       FirebaseUser user = auth.getCurrentUser();
       if (user != null) { //si connecté
            email.setText(user.getEmail());
            nom.setText(user.getDisplayName());
            contact.setText(ConnexionInfo.getContact(this, user.getEmail()));
       } else {
           Intent intent = new Intent(this, SoireesActivity.class);
           startActivity(intent);
           finish();
       }

   }
    public void deconnexion(View v) {

        ConnexionInfo.deconnecter(this);
    }

}

