package fr.minesparis.ae.caross.nouveauDepart;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import fr.minesparis.ae.caross.R;

/**
 * Created by Louis MEYRAT on 25/01/2017.
 *
 * Fragment qui demande la date du départ à l'utilisateur
 */

public class FragmentNDepartAccord extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_ndepart_accord, container, false);
        final Button ajouter = (Button)v.findViewById(R.id.ndepart_ajouter);
        ajouter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((NouveauDepartActivity)getActivity()).envoyerDepart();
            }
        });
        CheckBox accepte = (CheckBox)v.findViewById(R.id.ndepart_accepte);
        accepte.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ajouter.setEnabled(b);
            }
        });
        WebView webView = (WebView)v.findViewById(R.id.ndepart_accord);
        webView.loadDataWithBaseURL(null, getString(R.string.html_accord), "text/html", "UTF-8", null);
        webView.setBackgroundColor(0x01000000);
        return v;

    }




}
