package fr.minesparis.ae.caross.http;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;

import fr.minesparis.ae.caross.BuildConfig;
import fr.minesparis.ae.caross.connexion.ConnexionInfo;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Louis MEYRAT on 22/12/2016.
 *
 * En Java 7 , on ne peut pas mettre de méthodes statiques dans une interface.
 * Comme cette fonction sera utisée à chaque connexion avec le serveur, on la met à part.
 */

public class APIGetter {

    private static APIInterface api; //un singleton, pour éviter d'avoir à le reconstruire à chaque appel du serveur
    private static boolean valide = false;

    public static APIInterface getAPIservice() {
        if (api != null && valide) return api;


        //pour le debugging, on affiche les requêtes http dans la console
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.DEBUG?HttpLoggingInterceptor.Level.BODY: HttpLoggingInterceptor.Level.NONE);

        //pour ajouter le PHPSESSID dans les requêtes, on crée un iterceptor
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                if (!ConnexionInfo.phpsessid.isEmpty()) {
                    request = request.newBuilder()
                            .addHeader("Cookie", ConnexionInfo.phpsessid)
                            .build();
                }
                Response response = chain.proceed(request);
                List<String> cookies = response.headers("Set-Cookie");
                for (String cookie : cookies){
                    if (cookie != null && !cookie.isEmpty()) {
                        ConnexionInfo.phpsessid+=cookie;
                    }
                }
                return response;
            }
        })
                .addInterceptor(interceptor)
                .build();

        //initialisation du sérialiser json
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConnexionInfo.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        api = retrofit.create(APIInterface.class);
        valide = true;

        return api;
    }

    //Si l'url est changée dans les paramètres, il faut reconstruire api
    public static void invalidateURL(){
        valide = false;
    }
}
