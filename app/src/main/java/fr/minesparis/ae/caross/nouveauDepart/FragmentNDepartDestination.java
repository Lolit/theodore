package fr.minesparis.ae.caross.nouveauDepart;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.shawnlin.numberpicker.NumberPicker;

import fr.minesparis.ae.caross.R;
/**
 * Created by Louis MEYRAT on 25/01/2017.
 *
 * Fragment qui demande la date du départ à l'utilisateur
 */

public class FragmentNDepartDestination extends Fragment {

    public static final int PLACE_PICKER_REQUEST = 1;
    private TextView adresse;
    protected ProgressDialog progres;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_ndepart_destination, container, false);
        final Button placeButton = v.findViewById(R.id.ndepart_bouton_places);
        final Spinner transport = v.findViewById(R.id.ndepart_transport_choix);
        final EditText rdv = v.findViewById(R.id.ndepart_rdv);
        final EditText commentaire = v.findViewById(R.id.ndepart_commentaire);

        adresse = v.findViewById(R.id.ndepart_transport_adresse);

        NouveauDepartActivity act = ((NouveauDepartActivity)getActivity());
        if(!act.isNouveau()){
            adresse.setText(act.getDepart().getAdresse());
            transport.setSelection(act.getDepart().getTransport()-1);
            placeButton.setText(Integer.toString(act.getDepart().getPlacesTotales()-1));
            commentaire.setText(act.getDepart().getCommentaire());
            rdv.setText(act.getDepart().getRendezVous());
        }

        placeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final View dialogContent = inflater.inflate(R.layout.number_picker, null);
                new AlertDialog.Builder(getContext())
                        .setView(dialogContent)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                NumberPicker np = (NumberPicker) dialogContent.findViewById(R.id.ndepart_nbplaces);
                                placeButton.setText(String.valueOf(np.getValue()));
                            }
                        })
                        .setNegativeButton(R.string.cancel, null)
                        .setTitle(R.string.dialog_places_picker_titre)
                        .show();
            }
        });

        ImageButton map = (ImageButton)v.findViewById(R.id.ndepart_bouton_map);
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            1);

                }

                //on utilisel'API de google
                //mode d'emploi ici : https://developers.google.com/places/android-api/placepicker


                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                builder.setLatLngBounds(new LatLngBounds(new LatLng(48.833538, 2.243619), new LatLng(48.898604, 2.401136)));
                //on vériife si le GPS est dispo
                final LocationManager manager = (LocationManager) getActivity().getSystemService( Context.LOCATION_SERVICE );

                if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                    builder.setLatLngBounds(new LatLngBounds(new LatLng(48.833538, 2.243619), new LatLng(48.898604, 2.401136)));
                }
                try{
                    if(progres==null  || !progres.isShowing()){
                        progres = ProgressDialog.show(getContext(), "Chargement",
                                "Lancement des Services Google Play...", true);
                    }
                    getActivity().startActivityForResult(builder.build(FragmentNDepartDestination.this.getActivity()), PLACE_PICKER_REQUEST);
                } catch ( GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e ) {
                    Log.e( "PlacesAPI", e.getMessage() );
                    Crashlytics.log(0, "PlacesAPI", e.getMessage());
                    if(progres!=null)progres.dismiss();
                }
            }
        });

        Button suivant = v.findViewById(R.id.ndepart_suivant);
        suivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Test", "Toast !");
                if(adresse.getText().length()>0) {

                    NouveauDepartActivity act = ((NouveauDepartActivity) getActivity());
                    act.setPlaces(Integer.parseInt(placeButton.getText().toString())+1);
                    act.setTransport(transport.getSelectedItemPosition()+1);//+1 car la liste des transports commence à 1
                    act.setRDV(rdv.getText().toString());
                    act.setCommentaire(commentaire.getText().toString());
                    act.showFragment(new FragmentNDepartAccord(), true);

                }else{

                    Toast.makeText( getActivity(),R.string.ndepart_erreur_adresse, Toast.LENGTH_SHORT).show();
                }
            }
        });

        return v;

    }


    public void setAdresse(CharSequence s) {
        //cette méthode est appelé dès qu'on a reçu le résultat de GooglePlace
        adresse.setText(s);
    }
}
