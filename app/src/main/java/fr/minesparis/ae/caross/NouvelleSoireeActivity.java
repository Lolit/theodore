package fr.minesparis.ae.caross;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.Manifest;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.Date;

import fr.minesparis.ae.caross.connexion.ConnexionInfo;
import fr.minesparis.ae.caross.connexion.FileToolbox;
import fr.minesparis.ae.caross.gson.ConnexionReponse;
import fr.minesparis.ae.caross.http.APIGetter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.gms.location.places.ui.PlacePicker.getPlace;

public class NouvelleSoireeActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    public static final int PLACE_PICKER_REQUEST = 1, IMAGE_PICKER = 2, DEMANDE_PERMISSION_LECTURE=3;
    private TextView adresseT, date, heureDebut;
    private FloatingActionButton boutonCreerSoiree;
    protected ProgressDialog progres;
    private String adresse, ville;
    private int codePostal;
    private Uri imageURI;
    private EditText nom, organisateur, description;
    private Calendar calDebut;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nouvelle_soiree);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adresseT = findViewById(R.id.tv_adresse);
        date = findViewById(R.id.tv_date);
        heureDebut = findViewById(R.id.tv_heure_debut);
        nom = findViewById(R.id.edit_nom_soiree);
        organisateur = findViewById(R.id.edit_organisateur);
        description = findViewById(R.id.edit_description);
        boutonCreerSoiree = findViewById(R.id.boutonCreerSoiree);

        calDebut = Calendar.getInstance();
        if (savedInstanceState != null) {
            adresse = savedInstanceState.getString("adresse");
            ville = savedInstanceState.getString("ville");
            codePostal = savedInstanceState.getInt("codePostal");
            imageURI = savedInstanceState.getParcelable("imageURI");
            Date dateDebut = new Date(savedInstanceState.getLong("dateDebut"));
            if(dateDebut.after(new Date(300))){
                //si on avait enregistré une Date dans le savedInstaceState
                calDebut.setTime(dateDebut);
                heureDebut.setText(calDebut.get(Calendar.HOUR_OF_DAY) + ":" + calDebut.get(Calendar.MINUTE));
                date.setText(calDebut.get(Calendar.DAY_OF_MONTH) + "/" + calDebut.get(Calendar.MONTH) + "/" + calDebut.get(Calendar.YEAR));
            }
        }
        if (adresse != null && ville != null && codePostal != 0) {
            //TODO: essayer de normaliser à d'autres langues
            adresseT.setText(adresse + ", " + codePostal + " " + ville);
        }
    }

    //appelée losqu'on clique sur le bouton en forme de carte (via le xml)

    public void choisirAdresse(View v) {
        //on utilisel'API de google
        //mode d'emploi ici : https://developers.google.com/places/android-api/placepicker


        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        builder.setLatLngBounds(new LatLngBounds(new LatLng(48.833538, 2.243619), new LatLng(48.898604, 2.401136)));

        try {
            if (progres == null || !progres.isShowing()) {
                progres = ProgressDialog.show(this, "Chargement",
                        "Lancement des Services Google Play...", true);
            }
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            Log.e("PlacesAPI", e.getMessage());
            Crashlytics.log(0, "PlacesAPI", e.getMessage());
            if (progres != null) progres.dismiss();
        }
    }

    public void choisirImage(View v) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Choisir une image"), IMAGE_PICKER);
    }

    public void choisirDate(View v) {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    public void choisirDebut(View v) {
        TimePickerDialog tpd = TimePickerDialog.newInstance(this, true);
        tpd.show(getFragmentManager(), "Timepickerdialog");
    }
    private boolean demanderPermissionLecture(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                showSnack(R.string.demande_permission_lecture);

            }

            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    DEMANDE_PERMISSION_LECTURE);


            return false;
        }
        return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case DEMANDE_PERMISSION_LECTURE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    creerSoiree(boutonCreerSoiree);

                } else {

                    boutonCreerSoiree.setEnabled(true);
                }
                break;
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (progres != null) progres.dismiss(); //on annule la boîte de dialogue
            if (resultCode == RESULT_OK) {
                Place place = getPlace(this, data);

                if (place.getAddress() != null) {
                    String adr = place.getAddress().toString();
                    String[] tmp = adr.trim().split(", ");
                    if (tmp.length >= 2) {
                        this.adresse = tmp[0].trim();
                        String[] tmp2 = tmp[1].trim().split(" ");
                        if (tmp2.length < 2) adresseIncorrecte(adr);
                        try {
                            codePostal = Integer.parseInt(tmp2[0]);
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                            adresseIncorrecte(adr);
                        }
                        ville = tmp2[1].trim();
                    } else {
                        adresseIncorrecte(adr);
                    }
                    adresseT.setText(place.getAddress());
                } else {
                    Snackbar.make(adresseT, R.string.erreur_adresse, Snackbar.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == IMAGE_PICKER) {
            if (resultCode == RESULT_OK) {
                if(data.getData()!=null) {
                    imageURI = data.getData();
                    Log.d("image", imageURI.getPath());
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        date.setText(dayOfMonth + "/" + monthOfYear + "/" + year);
        calDebut.set(Calendar.YEAR, year);
        calDebut.set(Calendar.MONTH, monthOfYear);
        calDebut.set(Calendar.DAY_OF_MONTH, dayOfMonth);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        heureDebut.setText(hourOfDay + ":" + minute);
        calDebut.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calDebut.set(Calendar.MINUTE, minute);
        calDebut.set(Calendar.SECOND, second);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putString("ville", ville);
        savedInstanceState.putString("adresse", adresse);
        savedInstanceState.putInt("codePostal", codePostal);
        savedInstanceState.putParcelable("imageURI", imageURI);
        savedInstanceState.putLong("dateDebut", calDebut.getTime().getTime());

        super.onSaveInstanceState(savedInstanceState);
    }

    private void adresseIncorrecte(String adresse) {
        this.adresse = null;
        codePostal = 0;
        ville = null;

        Snackbar.make(adresseT, getString(R.string.erreur_adresse_contenu, adresse), Snackbar.LENGTH_SHORT).show();

    }

    private RequestBody createPartFromString(String msg) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, msg);
    }

    public void creerSoiree(View w){

        if(nom.length()<3||nom.length()>getResources().getInteger(R.integer.nom_soiree_max)) {
            showSnack(R.string.erreur_nom_soiree);
            return;
        }
        if(organisateur.length()<3||organisateur.length()>getResources().getInteger(R.integer.nom_organisateur_max)) {
            showSnack(R.string.erreur_nom_organisateur);
            return;
        }

        if(adresse==null||adresse.length()==0){
            showSnack(R.string.erreur_adresse_vide);
            return;
        }
        //adapté de https://futurestud.io/tutorials/retrofit-2-how-to-upload-files-to-server
        MultipartBody.Part bodyFile = null;
        if(imageURI!=null) {
            if(!demanderPermissionLecture())return;
            File file;
            try {
                file = new File(FileToolbox.getFilePath(getApplicationContext(), imageURI));
            } catch (URISyntaxException e) {
                e.printStackTrace();
                return;
            }

            // create RequestBody instance from file
            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse(getContentResolver().getType(imageURI)),
                            file
                    );

            // MultipartBody.Part is used to send also the actual file name
            bodyFile = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        }

        boutonCreerSoiree.setEnabled(false);

        // finally, execute the request
        Call<ConnexionReponse> call = APIGetter.getAPIservice().nouvelleSoiree(
                createPartFromString(nom.getText().toString()),
                createPartFromString(organisateur.getText().toString()),
                createPartFromString(adresse),
                createPartFromString(ville),
                createPartFromString(Integer.toString(codePostal)),
                createPartFromString(ConnexionInfo.DFH_FULL.format(calDebut.getTime())),
                createPartFromString(""),
                createPartFromString(description.getText().toString()),
                createPartFromString("true")
                , bodyFile);
        call.enqueue(new Callback<ConnexionReponse>() {
            @Override
            public void onResponse(@NonNull Call<ConnexionReponse> call,
                                   @NonNull Response<ConnexionReponse> response) {
                if(response.code()==200){
                    Log.v("Nouvelle soiree", "creation reussie");
                    Toast toast = Toast.makeText(NouvelleSoireeActivity.this, R.string.nouvelle_soiree_ok, Toast.LENGTH_SHORT);
                    toast.show();
                    finish();
                }else{
                    ConnexionReponse rep = response.body();
                    if(rep !=null)
                        showSnack(rep.getMessage());
                }
                boutonCreerSoiree.setEnabled(true);

            }

            @Override
            public void onFailure(@NonNull Call<ConnexionReponse> call, @NonNull Throwable t) {
                Log.e("Nouvelle soiree:", t.getMessage());
                showSnack(R.string.msg_internet_erreur);
                boutonCreerSoiree.setEnabled(true);
            }
        });
    }
    private void showSnack(int resMessage){
        Snackbar.make(adresseT, resMessage, Snackbar.LENGTH_SHORT).show();
    }
    private void showSnack(String message){
        Snackbar.make(adresseT, message, Snackbar.LENGTH_SHORT).show();
    }


    //Ne fonctionne pas (bug de la library ?
    /*
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }
    */
}
