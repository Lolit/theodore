package fr.minesparis.ae.caross.views;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import fr.minesparis.ae.caross.R;

/**
 * Created by Louis MEYRAT on 28/03/2017.
 *
 */

public class DialogReport extends DialogFragment {

    private SignalerListener listener;

    private TextInputLayout til;
    private EditText motif;
    private String nom;

    /**
     * Ce qui permet de connaitre l'acticvity qui appelle le dialogue
     * @param cont l'activity parrente, qui implémente
     */
    @Override
    public void onAttach(Context cont){
        super.onAttach(cont);
        try {
            listener = (SignalerListener) cont;
        } catch (ClassCastException e) {
            throw new ClassCastException(cont.toString()
                    + " must implement SignalerListener");
        }

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.dialog_signaler, null);

        motif = (EditText) layout.findViewById(R.id.report_motif);
        til = (TextInputLayout)layout.findViewById(R.id.report_input_layout) ;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.etat_report_titre)
                .setMessage(R.string.etat_report_message)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setView(layout)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .setNegativeButton(R.string.cancel, null);
        return builder.create();
    }


    //en effet, on ne veut pas fermer la boîte de dialogue à chaque appui sur valider : on remplace le listener du bouton

    @Override
    public void onStart()
    {
        super.onStart();
        this.setCancelable(false);
        final AlertDialog d = (AlertDialog)getDialog();
        if(d != null)
        {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                        if(motif.length()<20){
                            til.setError(getString(R.string.erreur_report));
                            return;
                        }
                        listener.signaler(nom, motif.getText().toString());


                    d.dismiss();
                }
            });
        }
    }
    public static DialogReport getInstance(String nom){
        DialogReport res = new DialogReport();
        res.nom = nom;
        return res;
    }

    public interface SignalerListener {
        void signaler(String nom, String motif);
    }
}
