package fr.minesparis.ae.caross.views;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class DropDownAnim extends Animation {

    /**
     * Classe troucée ici : http://stackoverflow.com/questions/4946295/android-expand-collapse-animation
     *
     * Animation permettant étendre et de rétrécir une View
     */

    private final int targetHeight;
    private final View view;
    private final boolean down;

    public DropDownAnim(View view, int targetHeight, boolean down) {
        this.view = view;
        this.targetHeight = targetHeight;
        this.down = down;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        if (down) {
            view.getLayoutParams().height = interpolatedTime == 1
                    ? ViewGroup.LayoutParams.WRAP_CONTENT
                    : (int)(targetHeight * interpolatedTime)+1;
        } else {
            if(interpolatedTime == 1){
                view.setVisibility(View.INVISIBLE);
            }else{
                view.getLayoutParams().height = targetHeight - (int)(targetHeight * interpolatedTime);
            }
        }
        view.requestLayout();
    }

    @Override
    public void initialize(int width, int height, int parentWidth,
            int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}