package fr.minesparis.ae.caross.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static android.content.ContentValues.TAG;

/**
 * Created by Louis MEYRAT on 15/03/2017.
 *
 * Gère les changement d'identifiant FCM (compte supprimé ...)
 */

public class InstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        //TODO : on pourrait l'envoyer au serveur, mais ce n'est pas la peine pour l'instant : je ne pense pas que l'id change pendant les quelques heures
        //TODO : d'utilisation de l'appli

    }
}
