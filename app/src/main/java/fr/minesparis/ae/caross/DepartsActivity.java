package fr.minesparis.ae.caross;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.List;

import fr.minesparis.ae.caross.connexion.ConnexionInfo;
import fr.minesparis.ae.caross.gson.Depart;
import fr.minesparis.ae.caross.gson.RequeteDeparts;
import fr.minesparis.ae.caross.http.APIGetter;
import fr.minesparis.ae.caross.http.APIInterface;
import fr.minesparis.ae.caross.notification.Tutoriel;
import fr.minesparis.ae.caross.nouveauDepart.NouveauDepartActivity;
import fr.minesparis.ae.caross.views.DepartsAdapter;
import me.toptas.fancyshowcase.FancyShowCaseQueue;
import me.toptas.fancyshowcase.FancyShowCaseView;
import me.toptas.fancyshowcase.FocusShape;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.gms.location.places.ui.PlacePicker.getPlace;

/**
 * Actuvity qui ressense les déprts d'une soirée.
 *
 * utilise des morceaux de code trouvés sur https://developers.google.com/places/android-api/start?hl=fr
 */
public class DepartsActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{

    //pour l'intent qui appelle cette activity
    public static final String IDSOIREE = "id_soiree";

    public static String[] TRANSPORTS;
    //Permet de convertir l'id des transports en une string dépendant de la langue de l'utilisateur

    private ListView listeDeparts;
    private DepartsAdapter adapter;
    private List<Depart> departs;
    private Spinner choixTransport;
    private SwipeRefreshLayout refresh;
    private ProgressDialog progres; //affiche une animation de chargement
    private FloatingActionButton fab;
    private Snackbar departEnCours; //pour que on puisse supprimer les SnackBar lorsque le  départ n'est plus en cours



    //la soiree dont on regarde les départs
    private int idSoiree = 0;

    private float longitude = 0, latitude = 0;

    //pour l'intent PLACE_PICKER
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final int NDEPART_INTENT = 2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TRANSPORTS = getResources().getStringArray(R.array.choix_transport);

        idSoiree = getIntent().getIntExtra(IDSOIREE, 0);

        setContentView(R.layout.activity_departs);

        setSupportActionBar(findViewById(R.id.departs_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        listeDeparts = findViewById(R.id.departs_liste);
        choixTransport = findViewById(R.id.depart_choix_transport);

        refresh = findViewById(R.id.swiperefresh);
        refresh.setOnRefreshListener(() -> getDeparts(latitude, longitude, choixTransport.getSelectedItemPosition()));

        departs = new ArrayList<>();
        adapter = new DepartsAdapter(DepartsActivity.this, departs);
        listeDeparts.setEmptyView(findViewById(R.id.departs_vide));
        listeDeparts.setAdapter(adapter);
        listeDeparts.setOnItemClickListener((arg0, v, position, arg3) -> {
            Intent intent = new Intent(DepartsActivity.this, DetailsDepartActivity.class);
            intent.putExtra(DetailsDepartActivity.ID_DEPART, departs.get(position).getId());
            intent.putExtra("latitude", latitude);
            intent.putExtra("longitude", longitude);
            startActivity(intent);
            //on lance une DetailsDepartActivity
        });
        choixTransport.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                getDeparts(latitude, longitude, position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //on ne fait rien
            }

        });
        fab = findViewById(R.id.departs_bouton_plus);
        fab.setOnClickListener(view -> {
            Intent intent = createNDepartIntent();
            startActivityForResult(intent, NDEPART_INTENT);

        });


    }
    @Override
    protected void onResume(){
        super.onResume();
        departEnCours = ConnexionInfo.afficherDepartEnCours(this, listeDeparts, departEnCours);
        fab.setVisibility((ConnexionInfo.isDepartEnCours(this))?View.GONE:View.VISIBLE);

        if(Tutoriel.tutorielDepartActivity){
            afficherTutoriel();
            Tutoriel.tutorielDepartActivity = false;
        }
    }

    @Override
    protected void onRestart(){
        //on veut remettre à zéro les paramètres de recherche
        super.onRestart();
        latitude = 0;
        longitude = 0;
        choixTransport.setSelection(0);
        getDeparts(latitude,longitude, choixTransport.getSelectedItemPosition());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_refresh:
                getDeparts(latitude,longitude, choixTransport.getSelectedItemPosition());
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    /**
     * Recherche et affiche les départs correspondants aux critères en paramètre
     * @param latitude latitude de la destination
     * @param longitude longitude de la destination
     * @param transport id du transport : utiliser TRANSPORTS
     */
    private void getDeparts(float latitude, float longitude, int transport){
        refresh.setRefreshing(true);
        APIInterface mApiService = APIGetter.getAPIservice();
        RequeteDeparts req = new RequeteDeparts(idSoiree, latitude, longitude, transport);
        Call<List<Depart>> mService = mApiService.getDeparts(req);
        mService.enqueue(new Callback<List<Depart>>() {
            @Override
            public void onResponse(Call<List< Depart>> call, Response<List<Depart>> response) {
                if (!response.isSuccessful()) {
                    Log.e("Connexion", response.message());
                    Snackbar.make(listeDeparts, "Erreur: réponse du serveur : "+response.code(), Snackbar.LENGTH_LONG).show();
                    return;
                }
                List<Depart> rep = response.body();//on obtient une instance de ConnexionReponse
                Log.d("info ", "Reçu code " + response.code());
                departs.clear();
                departs.addAll(rep);
                adapter.notifyDataSetChanged();
                refresh.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<List<Depart>> call, Throwable t) {
                refresh.setRefreshing(false);                call.cancel();
                Log.e("Connexion", "Erreur fatale de connexion");
                Snackbar.make(listeDeparts, R.string.msg_internet_erreur, Snackbar.LENGTH_LONG).show();
            }
        });

    }
    private void afficherTutoriel() {
        final FancyShowCaseView fancyShowCaseView1 = new FancyShowCaseView.Builder(this)
                .title("Ici s'affichent les annonces de tous ceux qui veulent rentrer depuis ta soirée")
                .focusShape(FocusShape.ROUNDED_RECTANGLE)
                .build();

        final FancyShowCaseView fancyShowCaseView2 = new FancyShowCaseView.Builder(this)
                .title("Tu peux filtrer les résultats par destination ou transport ici ")
                .focusShape(FocusShape.ROUNDED_RECTANGLE)
                .focusOn(findViewById(R.id.depart_toolbar_recherche))
                .build();
        final FancyShowCaseView fancyShowCaseView3 = new FancyShowCaseView.Builder(this)
                .title("Si aucun départ ne te va, tu peux en annoncer un nouveau ici")
                .focusOn(fab)
                .build();
        final FancyShowCaseView fancyShowCaseView4 = new FancyShowCaseView.Builder(this)
                .title("Appui sur un départ, pour voir la suite, ou crées-en un nouveau !")
                .focusShape(FocusShape.ROUNDED_RECTANGLE)
                .build();
        final FancyShowCaseQueue mQueue = new FancyShowCaseQueue()
                .add(fancyShowCaseView1)
                .add(fancyShowCaseView2)
                .add(fancyShowCaseView3)
                .add(fancyShowCaseView4);
        mQueue.show();
    }
    public void onBoutonMapClick(View v){
        //activation du gps pour Android >6, code trouvé sur
        // http://www.tutos-android.com/android-6-permissions
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);

        }

        //on utilisel'API de google
        //mode d'emploi ici : https://developers.google.com/places/android-api/placepicker

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        builder.setLatLngBounds(new LatLngBounds(new LatLng(48.833538, 2.243619), new LatLng(48.898604, 2.401136)));

        try {
            if (progres == null || !progres.isShowing()) {
                progres = ProgressDialog.show(this, "Chargement",
                        "Lancement des Services Google Play...", true);
            }
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            Log.e("PlacesAPI", e.getMessage());
            //FirebaseCrash.report(new Exception("Google API"));
            if (progres != null) progres.dismiss();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if(progres!=null && progres.isShowing())progres.dismiss();
            if (resultCode == RESULT_OK) {
                Place place = getPlace(this, data);
                departs.clear();
                longitude = (float)place.getLatLng().longitude;
                latitude = (float)place.getLatLng().latitude;
                getDeparts(latitude, longitude, choixTransport.getSelectedItemPosition());
            }
        }else if(requestCode == NDEPART_INTENT){
            //on réinitialise la recherche par lieux, pour être sûr de voir le départ que l'on vient de créer
            latitude = 0;
            longitude = 0;

            getDeparts(latitude, longitude, choixTransport.getSelectedItemPosition());
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        /*FirebaseCrash.logcat(Log.ERROR, "Google API ", connectionResult.getErrorMessage());
        FirebaseCrash.report(new Exception("API google"));*/
    }


    private Intent createNDepartIntent(){
        Intent intent = new Intent(this, NouveauDepartActivity.class);
        intent.putExtra("idSoiree", idSoiree);
        return intent;
    }
}
