package fr.minesparis.ae.caross;

import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.willowtreeapps.spruce.Spruce;
import com.willowtreeapps.spruce.animation.DefaultAnimations;
import com.willowtreeapps.spruce.sort.DefaultSort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.minesparis.ae.caross.connexion.ConnexionInfo;
import fr.minesparis.ae.caross.connexion.ProfilActivity;
import fr.minesparis.ae.caross.gson.ConnexionReponse;
import fr.minesparis.ae.caross.gson.EtatDepart;
import fr.minesparis.ae.caross.gson.Soiree;
import fr.minesparis.ae.caross.http.APIGetter;
import fr.minesparis.ae.caross.http.APIInterface;
import fr.minesparis.ae.caross.notification.Tutoriel;
import fr.minesparis.ae.caross.views.DialogContact;
import fr.minesparis.ae.caross.views.RecyclerViewEmptySupport;
import fr.minesparis.ae.caross.views.SoireesAdapter;
import io.fabric.sdk.android.Fabric;
import me.toptas.fancyshowcase.FancyShowCaseQueue;
import me.toptas.fancyshowcase.FancyShowCaseView;
import me.toptas.fancyshowcase.FocusShape;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SoireesActivity extends AppCompatActivity implements DialogContact.DialogContactListener, GoogleApiClient.OnConnectionFailedListener {

    //pour tester l'autentification
    private static final int RC_SIGN_IN = 123;

    //booleen de premier lancement
    private static boolean premierLancement=true;

    private String urlPrecedant = "";
    private MenuItem profil;
    private CoordinatorLayout layout;

    private FirebaseUser user;
    private RecyclerViewEmptySupport listeSoirees;
    private SoireesAdapter adapter;

    private static boolean animer = false;


    private ProgressDialog progres;

    private SwipeRefreshLayout refresh;

    private Snackbar departEnCours;


    public static boolean erreurConnexion = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soirees);

        Toolbar toolbar = findViewById(R.id.soirees_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setIcon(R.mipmap.ic_launcher);


        this.setTitle(R.string.title_activity_soirees);

        layout =  findViewById(R.id.soirees_layout);
        refresh =  findViewById(R.id.swiperefresh);
        refresh.setOnRefreshListener(() -> getSoirees());

        listeSoirees = findViewById(R.id.listeSoirees);
        List<Soiree> soirees = new ArrayList<>();
        adapter = new SoireesAdapter(soirees, listeSoirees);

        TextView soireesEmpty = findViewById(R.id.soirees_vide);
        listeSoirees.setEmptyView(soireesEmpty);
        listeSoirees.setAdapter(adapter);
        LinearLayoutManager llm = new LinearLayoutManager(this) {
            @Override
            public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
                super.onLayoutChildren(recycler, state);
                // Animate in the visible children
                if(animer) {
                    new Spruce.SpruceBuilder(listeSoirees)
                            .sortWith(new DefaultSort(100))
                            .animateWith(DefaultAnimations.shrinkAnimator(listeSoirees, 800),
                                    ObjectAnimator.ofFloat(listeSoirees, "translationX", -listeSoirees.getWidth(), 0f).setDuration(800))
                            .start();
                    animer = false;
                }
            }
        };
        Fabric.with(this, new Crashlytics());

        listeSoirees.setLayoutManager(llm);

    }
    @Override
    public void onResume(){
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        ConnexionInfo.initialiserPrefs(prefs);
        if(!urlPrecedant.equals(ConnexionInfo.BASE_URL)) {
            Log.d("Params", "changés");
            APIGetter.invalidateURL();
            urlPrecedant = ConnexionInfo.BASE_URL;
        }
        if(!ConnexionInfo.estConnecte) {
            FirebaseAuth auth = FirebaseAuth.getInstance();
            if (auth.getCurrentUser() != null) {
                user = auth.getCurrentUser();
                //si erreurConnection, on a déja affiché la boîte de dialogue
                String contact = ConnexionInfo.getContact(this, user.getEmail());
                /*if (!erreurConnexion && contact == null) {
                    new DialogContact().show(getSupportFragmentManager(), "DialogContact");
                }else{
                    onDialogValider(contact); TODO : mettre un message plus joli
                }*/
                onDialogValider("");
                if(premierLancement){
                    afficherMessageBienvenu();
                    premierLancement=false;
                }


            } else {
                // not signed in
                //on vérifie la disponibilité des APIS Google
                GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
                int status = googleApiAvailability.isGooglePlayServicesAvailable(this);
                if(status != ConnectionResult.SUCCESS) {
                    if (googleApiAvailability.isUserResolvableError(status)) {
                        googleApiAvailability.getErrorDialog(this, status, 2404).show();
                    }
                    Toast.makeText(getBaseContext(), "Les services GooglePlay ne sont pas à jour", Toast.LENGTH_LONG).show();
                    Log.e("GoogleAPI", "Les services GooglePlay ne sont pas disponibles");
                    Crashlytics.log(1, "GoogleAPI", "Les services GooglePlay ne sont pas disponibles");
                    return;
                }


                startActivityForResult(AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(
                             Arrays.asList(new AuthUI.IdpConfig.EmailBuilder().build(),
                                     new AuthUI.IdpConfig.GoogleBuilder().build(),
                                     new AuthUI.IdpConfig.FacebookBuilder().build()))
                        .setLogo(R.mipmap.logo)
                        .setTosUrl("http://keewit.fr/cgu.html")
                        .setIsSmartLockEnabled(!BuildConfig.DEBUG)
                        .build(),
                        RC_SIGN_IN);

            }
        }else{
            getSoirees();
        }
        departEnCours = ConnexionInfo.afficherDepartEnCours(this, layout, departEnCours);
        demanderAfficherTutoriel();

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //pour recevoir les résultats de la connexion de l'utilisateur
        super.onActivityResult(requestCode, resultCode, data);
        // on vérifie qu'on a bien le retour de l'activity de connexion
        Log.d("Connexion", "Resultat !");
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            // Connexion réussie !
            if (resultCode != RESULT_OK) {
                // Sign in failed
                if (resultCode==RESULT_CANCELED || response == null) {
                    // L'utilisateur est revenu en arrière
                    finish();
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.NO_NETWORK) {
                    showSnackbar(R.string.no_internet_connection);
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                    showSnackbar(R.string.unknown_error);
                    return;
                }
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // on charge le menu, et on utilise l'icone verte si on est déja connecté
        getMenuInflater().inflate(R.menu.menu_soirees, menu);
        profil = menu.findItem(R.id.menu_profil);
        if(ConnexionInfo.estConnecte && profil!=null){
            profil.setIcon(R.drawable.ic_profil_connecte);
        }
        return true;
    }

    private void getSoirees(){
        refresh.setRefreshing(true);
        Log.v("COnnexion", "Connexion à "+ConnexionInfo.BASE_URL);
        APIInterface mApiService = APIGetter.getAPIservice();
        Call<List<Soiree>> mService = mApiService.getSoirees();
        mService.enqueue(new Callback<List<Soiree>>() {
            @Override
            public void onResponse(Call<List< Soiree>> call, Response<List<Soiree>> response) {
                if (!response.isSuccessful()) {
                    Log.e("Connexion", response.message());
                    Snackbar.make(layout, "Erreur: réponse du serveur : "+response.code(), Snackbar.LENGTH_LONG).show();
                    return;
                }
                List<Soiree> rep = response.body();//on obtient une instance de ConnexionReponse
                Log.d("info ", "Reçu code " + response.code());
                adapter.getSoirees().clear();
                if(rep!=null)
                adapter.getSoirees().addAll(rep);
                adapter.resetDataSet();
                refresh.setRefreshing(false);
                animer = true;

            }

            @Override
            public void onFailure(Call<List<Soiree>> call, Throwable t) {
                call.cancel();
                Log.e("Connexion", "Erreur fatale de connexion");
                Snackbar.make(layout, R.string.msg_internet_erreur, Snackbar.LENGTH_LONG).show();
                refresh.setRefreshing(false);            }
        });

    }
    private void seConnecter(final String id, final String email, final String nom, final String contact) {
        if(progres==null  || !progres.isShowing()){
            progres = ProgressDialog.show(this, "Connexion",
                    "Connexion au serveur...", true);
        }
        APIInterface mApiService = APIGetter.getAPIservice();
        Call<ConnexionReponse> mService = mApiService.connexion(id, email, nom, contact);
        mService.enqueue(new Callback<ConnexionReponse>() {
            @Override
            public void onResponse(Call<ConnexionReponse> call, Response<ConnexionReponse> response) {
                if (response.body() == null) {
                    Log.e("connexion", "Reçu une réponse vide");
                    Snackbar.make(layout, "Erreur: réponse du serveur : " + (response == null ? "null" : response.code()), Snackbar.LENGTH_LONG).show();
                    erreurConnexion(id, email, nom, contact);
                    return;
                }
                ConnexionReponse rep = response.body();//on obtient une instance de ConnexionReponse
                Log.d("info ", "Reçu code " + response.code());
                String codeValidation = rep.getMessage();

                if (codeValidation.equals("Valide")) {
                    ConnexionInfo.setContact(SoireesActivity.this, contact, email);
                    ConnexionInfo.email = email;
                    ConnexionInfo.nom = nom;
                    ConnexionInfo.estConnecte=true;
                    erreurConnexion = false;
                    if(profil!=null)
                    profil.setIcon(R.drawable.ic_profil_connecte);
                    recupererDepartEnCours();

                } else {
                    erreurConnexion(id, email, nom, contact);
                }
                if(progres!=null)progres.dismiss();
                getSoirees();
            }

            @Override
            public void onFailure(Call<ConnexionReponse> call, Throwable t) {
                if(progres!=null)progres.dismiss();
                erreurConnexion(id, email, nom, contact);
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_options:
                startActivity(new Intent(this, PreferencesActivity.class));
                return true;
            case R.id.menu_profil:
                startActivity(new Intent(this, ProfilActivity.class));
                return true;
            case R.id.menu_refresh:
                getSoirees();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
    public void recupererDepartEnCours(){
        if(progres==null  || !progres.isShowing()){
            progres = ProgressDialog.show(this, "Synchronisation",
                    "Récupération des départs...", true);
        }
        APIInterface mApiService = APIGetter.getAPIservice();
        Call<EtatDepart> mService = mApiService.getDepartEnCours();
        mService.enqueue(new Callback<EtatDepart>() {
            @Override
            public void onResponse(Call<EtatDepart> call, @NonNull Response<EtatDepart> response) {
                if (response.body()==null && response.code()!=204) {

                        Log.e("connexion", "Reçu une réponse vide");
                        Snackbar.make(layout, "Erreur: réponse du serveur : " + (response.code()), Snackbar.LENGTH_LONG).show();
                        return;


                }
                if(response.code()==204) {//alors aucun départ n'est en cours, tout va bien
                    ConnexionInfo.setDepartEnCours(SoireesActivity.this, 0);

                    return;
                }
                //sinon, on a un départ en cours.

                EtatDepart rep = response.body();//on obtient une instance de EtatDepart
                Log.d("info ", "Reçu code " + response.code());

                ConnexionInfo.setDepartEnCours(SoireesActivity.this, rep.details.getHeure().getTime());
                Intent intent = new Intent(SoireesActivity.this, EtatDepartActivity.class);
                intent.putExtra(EtatDepartActivity.ETAT_DEPART, rep);
                startActivity(intent);
                if(progres!=null)progres.dismiss();

            }

            @Override
            public void onFailure(Call<EtatDepart> call, Throwable t) {
                if(progres!=null)progres.dismiss();
                Snackbar.make(layout, "Echec de la connexion au serveur", Snackbar.LENGTH_SHORT).show();
            }
        });
    }
    private void erreurConnexion(final String id, final String email, final String nom, final String contact){
        erreurConnexion = true;
        new AlertDialog.Builder(SoireesActivity.this)
                .setTitle("Erreur de connexion")
                .setMessage(R.string.soirees_connexion_error)
                .setPositiveButton(R.string.reessayer, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        seConnecter(id, email, nom, contact);
                    }
                })
                .setNegativeButton(R.string.action_deconnexion, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ConnexionInfo.deconnecter(SoireesActivity.this);
                        finish();
                    }
                })
                .setCancelable(BuildConfig.DEBUG)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    @Override
    public void onDialogValider(final String contact) {
        user.getIdToken(false)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        String idToken = task.getResult().getToken();
                        seConnecter( idToken, user.getEmail(), user.getDisplayName(), contact);
                    } else {
                        Log.e("Firebase", "Une erreur s'est produite lors de la création du token");
                    }
                });


    }
    @Override
    public void onPause(){
        super.onPause();
        if(progres!=null && progres.isShowing())progres.dismiss();
        progres = null;
    }
    //affiche un message d'erreur
    private void showSnackbar(int stringID){
        Snackbar.make(listeSoirees, stringID, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("GoogleAPI ", connectionResult.getErrorMessage());
    }
    private void afficherMessageBienvenu(){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        WebView wv = new WebView(this);
        wv.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                final Uri uri = request.getUrl();
                view.loadUrl(uri.toString());
                return true;
            }
        });
        wv.loadUrl(getResources().getString(R.string.bienvenue_url));

        alert.setView(wv);
        alert.setNegativeButton(R.string.ok, (dialog, id) -> dialog.dismiss());
        alert.show();
    }
    void demanderAfficherTutoriel(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Tutoriel.activerTutoriel(prefs.getBoolean(Tutoriel.TUTORIEL, true));
        if(Tutoriel.tutorielSoireeActivity){
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setMessage(R.string.tutoriel_demande_activation);
            alert.setPositiveButton(R.string.oui, (dialogInterface, i) -> demarrerTutoriel());
            alert.setNegativeButton(R.string.pas_maintenant, (dialog, i) -> {
                dialog.dismiss();
                Tutoriel.activerTutoriel(false);
            });
            alert.show();
            prefs.edit().putBoolean(Tutoriel.TUTORIEL, false).apply();
        }

    }
    void demarrerTutoriel(){
        if(Tutoriel.tutorielSoireeActivity) {
            final FancyShowCaseView fancyShowCaseView1 = new FancyShowCaseView.Builder(this)
                    .title("Toutes les soirées enregistrées s'affichent ici")
                    .titleStyle(R.style.Font_Titre_Noir, Gravity.CENTER)
                    .focusOn(refresh)
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .build();

            final FancyShowCaseView fancyShowCaseView2 = new FancyShowCaseView.Builder(this)
                    .title("Tu peux toi-même en ajouter à la liste en cliquant ici")
                    .focusShape(FocusShape.CIRCLE)
                    .focusOn(findViewById(R.id.bouton_nouvelle_soiree))
                    .build();
            final FancyShowCaseQueue mQueue = new FancyShowCaseQueue()
                    .add(fancyShowCaseView1)
                    .add(fancyShowCaseView2);
            mQueue.show();
        }
        Tutoriel.tutorielSoireeActivity = false;

    }
    public void onNouvelleSoireeClicked(View v){
        //for result pour actualiser ?
        startActivity(new Intent(this, NouvelleSoireeActivity.class));

    }

}
