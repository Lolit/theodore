package fr.minesparis.ae.caross.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.provider.Settings;
import android.support.annotation.Keep;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import fr.minesparis.ae.caross.R;

@Keep
public class SnackBehavior extends CoordinatorLayout.Behavior<View> {

    /**
     * Classe permettant de redimensionner les layouts quand une SnackBar apparaît
     *
     *
     */


    public SnackBehavior() {
        super();
    }

    public SnackBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
}


    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return dependency instanceof Snackbar.SnackbarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        DisplayMetrics dm = parent.getResources().getDisplayMetrics();
        ViewCompat.setPaddingRelative(child, child.getPaddingLeft(), child.getPaddingTop(), child.getPaddingRight(), (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, dm)+dependency.getHeight());
        return true;
    }

    //appelé quand on enlève la SnackBar
    public void onDependentViewRemoved(CoordinatorLayout parent, View child, View dependency) {
        DisplayMetrics dm = parent.getResources().getDisplayMetrics();
        ViewCompat.setPaddingRelative(child, child.getPaddingLeft(), child.getPaddingTop(), child.getPaddingRight() , (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, dm));
    }
}