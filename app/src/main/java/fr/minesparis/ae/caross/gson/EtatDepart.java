package fr.minesparis.ae.caross.gson;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import fr.minesparis.ae.caross.connexion.ConnexionInfo;

/**
 * Created by Louis MEYRAT on 19/02/2017.
 *
 * Permet d'afficher l'écran sur le départ en cours. Cette classe est parcelable, car
 * je veux la psser dans un Intent vers EtatDepartActivity
 */

public class EtatDepart implements Parcelable{

    public DetailsDepart details;
    public List<String> participants;
    public List<String> ids;

    public final static Parcelable.Creator<EtatDepart> CREATOR = new Creator<EtatDepart>() {


        public EtatDepart createFromParcel(Parcel in) {
            EtatDepart instance = new EtatDepart();
            instance.participants = new ArrayList<>();
            instance.ids = new ArrayList<>();
            in.readStringList(instance.participants);
            in.readStringList(instance.ids);

            instance.details = in.readParcelable(DetailsDepart.class.getClassLoader());
            return instance;
        }


        public EtatDepart[] newArray(int size) {
            return (new EtatDepart[size]);
        }

    };

    public EtatDepart(){
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeStringList(participants);
        dest.writeStringList(ids);
        dest.writeParcelable(details, i);
        Log.d("Debug", details.getHeure().toString());

    }
}
