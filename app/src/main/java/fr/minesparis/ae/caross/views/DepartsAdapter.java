package fr.minesparis.ae.caross.views;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fr.minesparis.ae.caross.R;
import fr.minesparis.ae.caross.gson.Depart;

/**
 * Created by Louis MEYRAT on 29/12/2016.
 *
 * Adapter de la listView des départs
 */

public class DepartsAdapter extends ArrayAdapter<Depart> {

    //Les ids des images à afficher
    private final int[] TRANSPORT_IMAGES_ID = {
            R.mipmap.ic_launcher,
            R.mipmap.ic_covoiturage,
            R.mipmap.ic_a_pied,
            R.mipmap.ic_uber,
            R.mipmap.ic_heetch,
            R.mipmap.ic_velo,
            R.mipmap.ic_commun,
            R.mipmap.ic_taxi,
            R.mipmap.ic_youngo};

    //departs est la liste à afficher
    public DepartsAdapter(Context context, List<Depart> departs) {
        super(context, 0, departs);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //on utilise un View Holder, pratique recommandée par Google

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.depart_elem, parent, false);
        }

        DepartsAdapter.DepartViewHolder viewHolder = (DepartsAdapter.DepartViewHolder) convertView.getTag();
        if (viewHolder == null) {
            viewHolder = new DepartsAdapter.DepartViewHolder();
            viewHolder.adresse = (TextView) convertView.findViewById(R.id.departs_adresse);
            viewHolder.heure = (TextView) convertView.findViewById(R.id.departs_heure);
            viewHolder.transportImage = (ImageView) convertView.findViewById(R.id.departs_transport);
            viewHolder.distance = (TextView)convertView.findViewById(R.id.departs_distance);
            viewHolder.places = (TextView)convertView.findViewById(R.id.departs_places);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List<Depart> departs
        Depart depart = getItem(position);

        //on modifie ce qui a changé
        viewHolder.adresse.setText(depart.getAdresse());
        viewHolder.heure.setText(depart.getHeure());
        viewHolder.transportImage.setImageResource(TRANSPORT_IMAGES_ID[depart.getTransport()]);

        if(depart.getDistance()>0.) {
            viewHolder.distance.setText(depart.getDistance() + "km");
        }else{
            viewHolder.distance.setText("");
        }
        if(depart.getPlacesUtilisees()>=depart.getPlacesTotales()){
            viewHolder.places.setTextColor(Color.RED);
        }else{
            viewHolder.places.setTextColor(Color.BLACK);
        }
        viewHolder.places.setText((depart.getPlacesTotales() - depart.getPlacesUtilisees())+"/"+depart.getPlacesTotales());
        return convertView;
    }

    private class DepartViewHolder {

        //permet de ne recréer que ce qui change lorsqu'une nouvelle vue apparait
        private TextView adresse;
        private TextView heure;
        private ImageView transportImage;
        private TextView distance;
        private TextView places;
    }
}
