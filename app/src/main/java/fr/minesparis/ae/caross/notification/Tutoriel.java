package fr.minesparis.ae.caross.notification;

/**
 * Created by Louis MEYRAT on 11/03/2018.
 *
 * Contient les constates et flags qui regissent l'affichage du tutoriel
 */

public class Tutoriel {

    public static final String TUTORIEL = "TUTORIEL_ON";

    public static boolean tutorielSoireeActivity = false;
    public static boolean tutorielDepartActivity = false;

    public static void activerTutoriel(boolean activer){
        tutorielDepartActivity = activer;
        tutorielSoireeActivity = activer;
    }

}
