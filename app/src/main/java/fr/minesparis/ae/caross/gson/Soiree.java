package fr.minesparis.ae.caross.gson;

import java.util.Date;

/**
 * Created by Louis MEYRAT on 13/12/2016.
 * Généré avec http://www.jsonschema2pojo.org/
 */

public class Soiree {

    public Soiree(int id, String nom, String adresse, String ville, String code_postal, Date heure_debut, Date heure_fin, String url_image, String auteur, String detailsURL) {
        this.id = id;
        this.nom = nom;
        this.adresse = adresse;
        this.ville = ville;
        this.code_postal = code_postal;
        this.heure_debut = heure_debut;
        this.heure_fin = heure_fin;
        this.url_image = url_image;
        this.auteur = auteur;
        this.detailsURL = detailsURL;
    }
    private int id;

    public int getId() { return this.id; }

    public void setId(int id) { this.id = id; }

    private String nom;

    public String getNom() { return this.nom; }

    public void setNom(String nom) { this.nom = nom; }

    private String adresse;

    public String getAdresse() { return this.adresse; }

    public void setAdresse(String adresse) { this.adresse = adresse; }

    private String ville;

    public String getVille() { return this.ville; }

    public void setVille(String ville) { this.ville = ville; }

    private String code_postal;

    public String getCodePostal() { return this.code_postal; }

    public void setCodePostal(String code_postal) { this.code_postal = code_postal; }


    private Date heure_debut;

    public Date getHeureDebut() { return this.heure_debut; }

    public void setHeureDebut(Date heure_debut) { this.heure_debut = heure_debut; }

    private Date heure_fin;

    public Date getHeureFin() { return this.heure_fin; }

    public void setHeureFin(Date heure_fin) { this.heure_fin = heure_fin; }

    private String url_image;

    public String getUrlImage() { return this.url_image; }

    public void setUrlImage(String url_image) { this.url_image = url_image; }

    private String auteur;

    public String getAuteur() { return this.auteur; }

    public void setAuteur(String auteur) { this.auteur = auteur; }

    public String detailsURL;


}
