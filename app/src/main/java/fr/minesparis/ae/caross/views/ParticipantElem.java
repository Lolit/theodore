package fr.minesparis.ae.caross.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import fr.minesparis.ae.caross.EtatDepartActivity;
import fr.minesparis.ae.caross.R;
import fr.minesparis.ae.caross.connexion.ConnexionInfo;

/**
 * Created by Louis MEYRAT on 04/03/2017.
 *
 * Un élément de la liste des participants qui s'affiche
 */

public class ParticipantElem extends FrameLayout{

    //couleur que doit prendre le champ de texte,qui change en fonction de à qui appartient le nom
    public enum Propriete{

        AUTEUR(R.color.couleurAuteur),
        UTILISATEUR(R.color.couleurUtilisateur),
        NORMALE(android.R.color.background_light);

        private int couleur;

        Propriete(int couleur){
            this.couleur=couleur;
        }
        public int getCouleur() {
            return couleur;
        }
    }

    View root, inroot; //servira à appeler toutes les view child;; pour le padding
    TextView nom;
    Propriete propriete;
    ImageButton signaler;



    public ParticipantElem(@NonNull Context context) {
        super(context);
    }

    public ParticipantElem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public static ParticipantElem getInstance(EtatDepartActivity context, String nom){
        ParticipantElem res = new ParticipantElem(context);
        res.init(context);
        res.setNom(nom);
        return res;
    }

    private void init(final EtatDepartActivity context){
        root = inflate(context, R.layout.elem_participants, this);
        inroot = (FrameLayout)findViewById(R.id.participant_in);
        nom = (TextView) root.findViewById(R.id.participant_nom);
        signaler = (ImageButton) root.findViewById(R.id.signaler);
        signaler.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
               DialogReport.getInstance(nom.getText().toString()).show(context.getSupportFragmentManager(), "REPORT");

            }
        });
        setPropriete(Propriete.NORMALE);
    }
    //modifie le nom du participant. Si ce nom est le même que celui de l'organisateur, enlève les boutons en trop
    public ParticipantElem setNom(String nom){
        this.nom.setText(nom);
        return this;
    }
    //ajoute des boutons si l'utilisateur est l'auteur du départ
    public void setAdmin(){
        if(propriete!=Propriete.UTILISATEUR) {
            ImageButton supprimer = (ImageButton) root.findViewById(R.id.supprimer);
            supprimer.setVisibility(View.VISIBLE);
            supprimer.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    //TODO : supprimer
                }
            });
        }
    }
    public ParticipantElem setPropriete(Propriete prop){
        propriete = prop;
        //deprecié, mais seule façon rétrocompatible
        //noinspection deprecation
        nom.setBackgroundColor(getContext().getResources().getColor(prop.getCouleur()));
        return this;
    }
    //signale que ce participant est l'utilisateur lui-même
    public void setUtilisateur(){
        //on enlève les boutons
        setPropriete(Propriete.UTILISATEUR);
        signaler.setVisibility(INVISIBLE);
    }
    public void setAmi(){
        Log.d("ami", "test");
        inroot.setPadding(inroot.getPaddingLeft()+50, inroot.getPaddingTop(), inroot.getPaddingRight(), inroot.getPaddingBottom());
    }



}
