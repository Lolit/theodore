package fr.minesparis.ae.caross.gson;

/**
 * Created by Louis MEYRAT on 05/02/2017.
 *
 * Le code d'une erreur à afficher, ou 0 si tout s'est bien passé
 */

public class CodeErreur {

    public int code;

    public CodeErreur(){}
}
