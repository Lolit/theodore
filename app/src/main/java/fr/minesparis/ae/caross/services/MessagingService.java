package fr.minesparis.ae.caross.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import fr.minesparis.ae.caross.R;
import fr.minesparis.ae.caross.SoireesActivity;
import fr.minesparis.ae.caross.connexion.ConnexionInfo;

/**
 * Created by Louis MEYRAT on 15/03/2017.
 *
 * Gère les messages reçus de Firebase Cloud Messagin, tourne en arrière plan de l'application
 */

public class MessagingService extends FirebaseMessagingService {

    public static final String NOTIFICATION = "fr.minesparis.ae.caross.services.receiver";
    public final static String CODE = "CODE";
    public enum Code{ //contenu du message, transmis aux Activity
        RIEN(0), DEPART_ANNULE(1), REFRESH(2), MODIF(3);

        private final int value;
        Code(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage.getNotification() != null) {
            sendNotification(remoteMessage); //si il y a une notifiaction dans le message, on l'affiche
        }

        //si il y a des données associées au message
        if (remoteMessage.getData().size() > 0) {
            Map<String, String> data = remoteMessage.getData();
            String code = data.get("CODE");
            if(code==null)return;
            switch (code){
                case "1":
                    //si le départ a été supprimé

                    //pour aficher un message, il faut retourner dans le Thread graphique principal
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast alerte = Toast.makeText(MessagingService.this, "Votre départ a été annulé par son auteur !", Toast.LENGTH_LONG);
                            alerte.getView().setBackgroundColor(Color.rgb(250, 100, 100));
                            ((TextView) alerte.getView().findViewById(android.R.id.message)).setTextColor(Color.YELLOW); //on arrache la rétine de l'utilisateur

                            alerte.show();
                            //on diffuse un intent, que toute activity à l'écoute interceptera (ici, on veut que EtatDepartActivity l'intercepte
                            Intent intent = new Intent(NOTIFICATION);
                            intent.putExtra(CODE, Code.DEPART_ANNULE.getValue());
                            sendBroadcast(intent);
                        }
                    } );
                    ConnexionInfo.setDepartEnCours(this, 0);
                    break;
                case "3":
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                    try {
                        Date heure = df.parse(data.get("HEURE"));
                        ConnexionInfo.setDepartEnCours(getApplicationContext(), heure.getTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    //pas de break
                case "2":
                    //on diffuse un intent, que toute activity à l'écoute interceptera (ici, on veut que EtatDepartActivity l'intercepte
                    Intent intent = new Intent(NOTIFICATION);
                    intent.putExtra(CODE, Code.REFRESH.getValue());
                    sendBroadcast(intent);
                    break;
            }
        }


    }

    private void sendNotification(RemoteMessage remoteMessage) {
        //on doit créer manuellement la notification quand l'appli est au premier plan
        Intent intent = new Intent(this, SoireesActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 423 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);


        Notification notif = new NotificationCompat.Builder(this)
                .setContentText(remoteMessage.getNotification().getBody())
                .setSmallIcon(R.drawable.ic_notification)
                .setAutoCancel(false)
                .setContentTitle(remoteMessage.getNotification().getTitle())
                .setContentIntent(pendingIntent)
                .build();
        notif.defaults |= Notification.DEFAULT_ALL;
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(423 /* ID of notification */, notif);
    }
}
