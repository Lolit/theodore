package fr.minesparis.ae.caross.views;

import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import fr.minesparis.ae.caross.R;

/**
 * Created by Louis MEYRAT on 04/02/2017.
 *
 * Dialogue qui demande un moyen de contact à l'utilisateur
 */

public class DialogContact extends DialogFragment {

    DialogContactListener listener;

    private CheckBox refus;
    private EditText contact;

    /**
     * Ce qui permet de connaitre l'acticvity qui appelle le dialogue
     * @param cont l'activity parrente, qui implémente
     */
    @Override
    public void onAttach(Context cont){
        super.onAttach(cont);
        try {
            listener = (DialogContactListener) cont;
        } catch (ClassCastException e) {
            throw new ClassCastException(cont.toString()
                    + " must implement DialogContactListener");
        }

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.dialog_contact, null);

        contact = (EditText) layout.findViewById(R.id.contact_text);
        contact.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        refus = (CheckBox) layout.findViewById(R.id.contact_non);
        builder.setView(layout)
                // Add action buttons
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {


                    }
                });

        builder.setMessage(R.string.contact_message);
        builder.setTitle(R.string.contact_titre);
        return builder.create();
    }

    public interface DialogContactListener {
        void onDialogValider(String contact);
    }
    //en effet, on ne veut pas fermer la boîte de dialogue à chaque appui sur valider : on remplace le listener du bouton

    @Override
    public void onStart()
    {
        super.onStart();
        this.setCancelable(false);
        final AlertDialog d = (AlertDialog)getDialog();
        if(d != null)
        {
            Button positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {


                    if(refus.isChecked()){
                        listener.onDialogValider("");
                    }else{
                        if(contact.length()<4){
                            contact.setError(getString(R.string.erreur_contact));
                            return;
                        }
                        listener.onDialogValider(contact.getText().toString());
                    }

                    d.dismiss();
                }
            });
        }
    }
}
