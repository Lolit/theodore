package fr.minesparis.ae.caross.gson;

/**
 * Created by Louis MEYRAT on 29/12/2016.
 *
 * Sera sérialisé en JSON et envoyé au serveur pour demander la liste des départs
 *
 */

public class RequeteDeparts {

    private int idSoiree;
    private float latitude;
    private float longitude;
    private int transport;


    public RequeteDeparts(int idSoiree, float latitude, float longitude, int transport) {
        this.idSoiree = idSoiree;
        this.latitude = latitude;
        this.longitude = longitude;
        this.transport = transport;
    }
}
