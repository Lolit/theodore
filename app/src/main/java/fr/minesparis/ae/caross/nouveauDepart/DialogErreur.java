package fr.minesparis.ae.caross.nouveauDepart;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import fr.minesparis.ae.caross.R;

/**
 * Created by Louis MEYRAT on 05/02/2017.
 *
 * Affiche les erreurs lors de la création d'un nouveau départ
 */
public class DialogErreur extends DialogFragment {

    //on n'aime vraiment pas les constructeurs en Android
    public static DialogErreur newInstance(int codeErreur){
        Bundle bundle = new Bundle();
        bundle.putInt("code", codeErreur);
        DialogErreur dE = new DialogErreur();
        dE.setArguments(bundle);
        return dE;
    }

    @Override
    public @NonNull Dialog onCreateDialog( Bundle savedInstanceState) {
        int code = getArguments().getInt("code", 0); //on récupère les paramètres
        if(!(code>0 && code<= 5))code = 5;
        String erreur = getResources().getStringArray(R.array.ndepart_erreurs)[code];
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(erreur)
                .setTitle(R.string.erreur)
                .setPositiveButton(R.string.ok, null);
        return builder.create();
    }
}
