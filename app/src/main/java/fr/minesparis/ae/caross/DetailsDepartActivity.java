package fr.minesparis.ae.caross;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.iid.FirebaseInstanceId;

import fr.minesparis.ae.caross.connexion.ConnexionInfo;
import fr.minesparis.ae.caross.gson.ConnexionReponse;
import fr.minesparis.ae.caross.gson.DetailsDepart;
import fr.minesparis.ae.caross.http.APIGetter;
import fr.minesparis.ae.caross.http.APIInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsDepartActivity extends AppCompatActivity implements OnMapReadyCallback {
    //s'appelle par un Intent qui a un extra ID_DEPART

    public static final String ID_DEPART = "id_depart";
    private DetailsDepart infos;
    private int idDepart;
    private TextView commentaires;
    private ProgressDialog progres;
    private Button rejoindre;

    private Snackbar departEnCours;

    private MapView map;
    private GoogleMap gMap;
    private MarkerOptions destination;

    private static final int REQUETE_PERMISSION_GPS = 1;
    private LatLng posDemandee;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_depart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.details_depart__toolbar);
        setSupportActionBar(toolbar);

        idDepart = getIntent().getIntExtra(ID_DEPART, 0);
        float lat = getIntent().getFloatExtra("latitude", 0);
        float lon = getIntent().getFloatExtra("longitude", 0);
        //on vérifie que c'est une vraie position, et pas un champ vide ou un bug
        if (lat != 0 && lon != 0) {
            posDemandee = new LatLng(lat, lon);
        }

        commentaires = (TextView) findViewById(R.id.details_depart_commentaire);

        getInformations();

        map = (MapView) findViewById(R.id.details_depart_map);
        map.onCreate(savedInstanceState);
        map.getMapAsync(this);

        rejoindre = (Button) findViewById(R.id.details_depart_rejoindre);
        rejoindre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inscrire();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_refresh, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_refresh:
                getInformations();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getInformations() {
        //récupère les informations du serveur, puis les affiche
        if(progres==null  || !progres.isShowing()){
            progres = ProgressDialog.show(this, "Connexion",
                    "Chargement des données...", true);
        }
        APIInterface mApiService = APIGetter.getAPIservice();
        Call<DetailsDepart> mService = mApiService.getDepartInfo(idDepart);
        mService.enqueue(new Callback<DetailsDepart>() {
            @Override
            public void onResponse(Call<DetailsDepart> call, Response<DetailsDepart> response) {
                if (!response.isSuccessful()) {
                    Log.e("Connexion", response.message());
                    Snackbar.make(commentaires, "Erreur: réponse du serveur : " + response.code(), Snackbar.LENGTH_LONG).show();
                    return;
                }
                infos = response.body();//on obtient une instance de ConnexionReponse

                //on affiche ces informations
                TextView tv = (TextView) findViewById(R.id.details_depart_auteur);
                //tv.setText(getString(R.string.details_depart_texte_presentation, ConnexionInfo.getHeureFormat().format(infos.heure), DepartsActivity.TRANSPORTS[infos.transport], infos.adresse, infos.distance));
                tv.setText(infos.getNom());

                tv = (TextView) findViewById(R.id.details_departs_heure);
                tv.setText(ConnexionInfo.getHeureFormat().format(infos.getHeure()));

                tv = (TextView) findViewById(R.id.details_depart_rdv);
                tv.setText(infos.getRendezVous());

                tv = (TextView) findViewById(R.id.details_depart_places);
                tv.setText(getString(R.string.details_depart_places, infos.getPlacesTotales()-infos.getPlacesUtilisees(), infos.getPlacesTotales()));

                if(infos.getPlacesTotales()<=infos.getPlacesUtilisees()){
                    rejoindre.setEnabled(false);
                    rejoindre.setText(R.string.details_depart_plus_de_places);
                    tv.setTextColor(Color.RED);
                }else{
                    if(ConnexionInfo.isDepartEnCours(DetailsDepartActivity.this)){
                        rejoindre.setEnabled(false);
                        rejoindre.setText(R.string.details_depart_deja_inscrit);
                    }else{
                        rejoindre.setEnabled(true);
                        rejoindre.setText(R.string.details_depart_rejoindre);
                    }

                    tv.setTextColor(Color.BLACK);
                }



                commentaires.setText(infos.getCommentaire());

                tv = (TextView) findViewById(R.id.details_depart_transport);
                tv.setText(DepartsActivity.TRANSPORTS[infos.getTransport()]);

                tv = (TextView) findViewById(R.id.details_depart_adresse);
                tv.setText(infos.getAdresse());

                findViewById(R.id.details_depart_appeler).setVisibility((infos.getContact().length()>=8)?View.VISIBLE:View.GONE);

                destination = new MarkerOptions();
                destination.position(new LatLng(infos.getLat(), infos.getLon()));
                destination.title(getString(R.string.departs_destination));
                if (gMap != null) {
                    //si la carte s'est chargé avant, on ajoute le marqueur ici
                    gMap.addMarker(destination);
                }

                Log.d("info ", "Reçu code " + response.code());
                centrerCamera();
                if(progres!=null)progres.dismiss();


            }

            @Override
            public void onFailure(Call<DetailsDepart> call, Throwable t) {
                if(progres!=null)progres.dismiss();

                call.cancel();
                Log.e("Connexion", "Erreur fatale de connexion");
                Snackbar.make(commentaires, R.string.msg_internet_erreur, Snackbar.LENGTH_LONG).show();
            }
        });

    }
    private void inscrire(){
        //s'inscrit au départ
        APIInterface mApiService = APIGetter.getAPIservice();
        Call<ConnexionReponse> mService = mApiService.inscrire(idDepart, FirebaseInstanceId.getInstance().getToken());
        mService.enqueue(new Callback<ConnexionReponse>() {

            @Override
            public void onResponse(Call<ConnexionReponse> call, Response<ConnexionReponse> response) {
                if (!response.isSuccessful()) {
                    Log.e("Connexion", response.message());
                    Snackbar.make(commentaires, "Erreur: réponse du serveur : " + response.code(), Snackbar.LENGTH_LONG).show();
                    return;
                }
                ConnexionReponse rep = response.body();
                if (rep.getMessage().equals("Valide")){
                    ConnexionInfo.setDepartEnCours(DetailsDepartActivity.this, infos.getHeure().getTime());
                    new AlertDialog.Builder(DetailsDepartActivity.this)
                            .setTitle(R.string.dialogue_inscription_titre)
                            .setMessage(R.string.dialogue_inscription_message)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setPositiveButton(R.string.ok, null)
                            .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    Intent intent = new Intent(DetailsDepartActivity.this, EtatDepartActivity.class);
                                    startActivity(intent);
                                }
                            })
                            .show();

                }else if(rep.getMessage().equals("places")){//message du serveur disant qu'il n'y a plus de places, si quelqu'un s'est inscrip juste avant
                    Snackbar.make(commentaires, R.string.plus_de_places, Snackbar.LENGTH_LONG).show();
                    getInformations();
                }
            }

            @Override
            public void onFailure(Call<ConnexionReponse> call, Throwable t) {
                call.cancel();
                Log.e("Connexion", "Erreur fatale de connexion");
                Snackbar.make(commentaires, R.string.msg_internet_erreur, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onResume() {
        map.onResume();
        super.onResume();
        departEnCours = ConnexionInfo.afficherDepartEnCours(this, map, departEnCours);
        getInformations();

    }

    @Override
    public void onPause() {
        super.onPause();
        map.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        map.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        map.onLowMemory();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        gMap = googleMap;
        activerPosition();
        if (destination != null)
            gMap.addMarker(destination);
        if (posDemandee != null)
            gMap.addMarker(new MarkerOptions().position(posDemandee)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                    .title(getString(R.string.details_depart_destination_demandee)));
    }

    public void activerPosition() {
        //pour activer le GPS sur ANdroid>6
        if (gMap != null) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                gMap.setMyLocationEnabled(true);
                Log.d("Permission", "permission accordée");


            } else {
                Log.d("Permission", "demande du gps");
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUETE_PERMISSION_GPS);
            }
            centrerCamera();
        }

    }

    /**
     * recentre la caméra pour voir les deux marqueure
     */
    private void centrerCamera() {
        if (gMap != null && destination != null) {
            if (posDemandee != null) {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(destination.getPosition());
                builder.include(posDemandee);
                LatLngBounds bounds = builder.build();
                int padding = 60; //60 pixels de marge
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                gMap.moveCamera(cu);
            } else {
                gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(destination.getPosition(), 15));
            }

        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        //pendant de activerGPS
        switch (requestCode) {
            case REQUETE_PERMISSION_GPS: {
                // Si la permission est annulée, grantResults est vide
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    activerPosition();
                }

            }

        }
    }
    //pour le bouton appeler
    public void appelerAuteur(View v){
        infos.envoyerSMSAuteur(this);
    }


}
