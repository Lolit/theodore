package fr.minesparis.ae.caross;

import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Calendar;
import java.util.Locale;

import fr.minesparis.ae.caross.connexion.ConnexionInfo;
import fr.minesparis.ae.caross.gson.ConnexionReponse;
import fr.minesparis.ae.caross.gson.EtatDepart;
import fr.minesparis.ae.caross.http.APIGetter;
import fr.minesparis.ae.caross.http.APIInterface;
import fr.minesparis.ae.caross.nouveauDepart.NouveauDepartActivity;
import fr.minesparis.ae.caross.services.MessagingService;
import fr.minesparis.ae.caross.views.DialogReport;
import fr.minesparis.ae.caross.views.DropDownAnim;
import fr.minesparis.ae.caross.views.ParticipantElem;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static fr.minesparis.ae.caross.services.MessagingService.CODE;
import static fr.minesparis.ae.caross.services.MessagingService.Code;

public class EtatDepartActivity extends AppCompatActivity implements DialogReport.SignalerListener{
    /**
     * Affiche les informations sur le départ en cours
     */


    private TextView participantsTitre;
    private EtatDepart etat;
    private ProgressDialog progres;
    private ImageButton appeler;
    private LinearLayout listeParticipants;
    private ImageView expandButton;

    //pour connuniquer avec les services en arrière plan
    private BroadcastReceiver receiver;

    public static final String ETAT_DEPART ="ETAT_DEPART";
    private int rotationAngle = 0; //pour le bouton qui sert à étendre la liste


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_etat_depart);
        setSupportActionBar((Toolbar) findViewById(R.id.etat_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        appeler = findViewById(R.id.etat_depart_appeler);
        participantsTitre = findViewById(R.id.etat_participants_titre);
        expandButton = findViewById(R.id.etat_expand);

        listeParticipants = findViewById(R.id.etat_listeParticipants);

        //si on a passé un état en paramètre, alors on l'affiche plut^t que de le redemander au serveur
        etat = getIntent().getParcelableExtra(ETAT_DEPART);

        if(etat == null) { //si on n'a pas un EtatDepart dans l'intent, on le demande au serveur
            getInformations();
        }else{
            updateData(etat);
        }

        receiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                int code = intent.getIntExtra(CODE, Code.RIEN.getValue());
                if(code == Code.DEPART_ANNULE.getValue()){
                    finish();
                }else if(code == Code.REFRESH.getValue()){
                    getInformations();
                }
            }
        };


    }

    @Override
    protected void onResume(){
        super.onResume();
        registerReceiver(receiver, new IntentFilter(
                MessagingService.NOTIFICATION));
    }
    @Override
    protected void onPause(){
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_refresh, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_refresh:
                getInformations();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void getInformations() {
        //se connecte au serveur pour récupérer les informations
        if(progres==null  || !progres.isShowing()){
            progres = ProgressDialog.show(this, "Connexion",
                    "Chargement des données...", true);
        }
        APIInterface mApiService = APIGetter.getAPIservice();
        Call<EtatDepart> mService = mApiService.getDepartEnCours();
        mService.enqueue(new Callback<EtatDepart>() {
            @Override
            public void onResponse(Call<EtatDepart> call, Response<EtatDepart> response) {
                if (!response.isSuccessful()) {
                    Log.e("Connexion", response.message());
                    Snackbar.make(participantsTitre, "Erreur: réponse du serveur : " + response.code(), Snackbar.LENGTH_LONG).show();
                    return;
                }
                if(response.code() == 204){ //si il n'y a pas de départ en cours
                    ConnexionInfo.setDepartEnCours(EtatDepartActivity.this, 0);
                    finish();
                }else {
                    etat = response.body();

                    updateData(etat);
                    if (progres != null && progres.isShowing()) progres.dismiss();
                }

            }

            @Override
            public void onFailure(Call<EtatDepart> call, Throwable t) {
                if(progres!=null && progres.isShowing())progres.dismiss();
                call.cancel();
                Log.e("Connexion", "Erreur fatale de connexion");
                new AlertDialog.Builder(EtatDepartActivity.this)
                        .setTitle("Erreur de connexion")
                        .setMessage(R.string.msg_internet_erreur_dialog)
                        .setPositiveButton(R.string.reessayer, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                getInformations();
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setCancelable(false)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();            }
        });

    }

    private void updateData(final EtatDepart etat) {
        //affiche etat
        TextView tv = (TextView) findViewById(R.id.etat_heure);
        tv.setText(getString(R.string.etat_heure, ConnexionInfo.getHeureFormat().format(etat.details.getHeure())));


        tv = (TextView) findViewById(R.id.details_depart_rdv);
        tv.setText(etat.details.getRendezVous());

        appeler.setVisibility((etat.details.getContact().length()>=8)?View.VISIBLE:View.INVISIBLE);

        participantsTitre.setText(getString(R.string.etat_participants , etat.details.getPlacesUtilisees(), etat.details.getPlacesTotales()));
        listeParticipants.removeAllViews();


        String nomUtilisateur = FirebaseAuth.getInstance().getCurrentUser().getDisplayName();

        ViewStub stub = (ViewStub) findViewById(R.id.etat_stub_boutons);
        //on ne charge pas les mêmes boutons selon si on est l'administrateur du départ ou un simple utilisateur

        //on vérifie si on est l'admin du départ, en considérant notre nom
        //TODO gérer le cas où deux utilisateurs ont le même nom
        boolean admin = etat.details.getNom().equals(nomUtilisateur);

        if (stub!=null) {
            if (admin) {
                stub.setLayoutResource(R.layout.etat_boutons_admin);
            } else {
                stub.setLayoutResource(R.layout.etat_boutons_user);
            }
            stub.inflate();
        }

        //à cette étape, le stub vaux forcément null, et le layout des boutons est inflaté

        ParticipantElem pe = ParticipantElem.getInstance(this, etat.details.getNom());
        pe.setPadding(pe.getPaddingLeft(), pe.getPaddingTop(), pe.getPaddingRight(), pe.getPaddingBottom()*2);//on crée un espace supplémentaire pour le nom de l'auteur


        if(admin){
            pe.setUtilisateur();
            Button lancerApp = (Button)findViewById(R.id.etat_lancer_app);
            final String application = getResources().getStringArray(R.array.application_transport)[etat.details.getTransport()];

            //on crée l'intent que va lancer ce bouton
            if(application.equals("Google Map")){
                lancerApp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //on lance googleMap
                        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", etat.details.getLat(), etat.details.getLon());
                        Intent intentApp = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        startActivity(intentApp);

                    }

                });
                lancerApp.setText(getString(R.string.etat_lancer_app, application));

            }else{
                final String[] app = application.split("\\|"); //regardez comment est fait R.array.application_transport
                lancerApp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //on lance l'application demandée, ou on l'affiche sur le play store si elle n'est pas installée
                        Intent intentApp = getPackageManager().getLaunchIntentForPackage(app[1]);
                        if(intentApp != null) { //null si l'application n'est pas trouvée
                            startActivity(intentApp);
                        }else{
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + app[1])));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                //oui, on peut ne pas avoir installé le PlayStore
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + app[1])));
                            }
                        }

                    }
                });
                lancerApp.setText(getString(R.string.etat_lancer_app, app[0]));
            }
        }

        pe.setPropriete(ParticipantElem.Propriete.AUTEUR);


        listeParticipants.addView(pe);

        String participant;
        String id_prec = "";
        String id;
        //pour savoir si un participant est ami d'un autre, on compare les ids
        for (int i = 0; i<etat.participants.size(); i++){
            participant = etat.participants.get(i);
            id = etat.ids.get(i);
            Log.d("ami", id);
            pe = ParticipantElem.getInstance(this, participant);

            if(participant.equals(nomUtilisateur)){
                pe.setUtilisateur();
            }

            if(id.equals(id_prec)){//si participant est ami du précédent
                pe.setAmi();
            }
            if(id.equals(etat.details.getIdAuteur())){
                pe.setAmi();
                Log.d("AMi", participant);
                listeParticipants.addView(pe, 1);//on l'ajoute juste sous le nom de l'auteur
                continue;
            }

            listeParticipants.addView(pe);
            id_prec = id;
        }
        listeParticipants.requestLayout();

        listeParticipants.invalidate();
        //on change la taille du parent (ce n'est pas fait automatiquement)

        Calendar c = Calendar.getInstance();
        Log.d("DepartDate", ""+ (c.getTimeInMillis()));
       /* new CountDownTimer(etat.details.getHeure().getTime() -c.getTimeInMillis() , 60000) {

            public void onTick(long tempsRestant) {
                chrono.setText(getString(R.string.heure_format, tempsRestant/3600000, tempsRestant/1000%3600/60));
            }

            public void onFinish() {
                chrono.setText(R.string.etat_temps_ecoule);
                chrono.setTextColor(Color.RED);
            }
        }.start();*/
    }
    public void signaler(String nom, String motif){
        //TODO: signaler l'id
        if(progres==null  || !progres.isShowing()){
            progres = ProgressDialog.show(this, "Connexion",
                    "Chargement des données...", true);
        }
        APIInterface mApiService = APIGetter.getAPIservice();
        Call<Void> mService = mApiService.signaler(nom, motif);
        mService.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (progres != null && progres.isShowing()) progres.dismiss();
                if (!response.isSuccessful()) {
                    Log.e("Connexion", response.message());
                    Snackbar.make(participantsTitre, "Erreur: réponse du serveur : " + response.code(), Snackbar.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                if (progres != null && progres.isShowing()) progres.dismiss();
                Log.d("dialogue", "fermé");
                Snackbar.make(participantsTitre, "Erreur de connexion à Internet", Snackbar.LENGTH_SHORT);
            }
        });

    }
    public void onDesinscrireClick(View v){
        new AlertDialog.Builder(this)
                .setTitle("Quitter le départ")
                .setMessage(R.string.etat_desinscrire_message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        seDesinscrire();
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .show();

    }
    public void onSupprimerClick(View v){
        new AlertDialog.Builder(this)
                .setTitle("Supprimer le départ")
                .setMessage(R.string.etat_supprimer_message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        supprimerDepart();
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .show();

    }
    private void supprimerDepart(){
        APIInterface mApiService = APIGetter.getAPIservice();
        Call<ConnexionReponse> mService = mApiService.quitterDepart();
        mService.enqueue(new Callback<ConnexionReponse>() {

            @Override
            public void onResponse(Call<ConnexionReponse> call, retrofit2.Response<ConnexionReponse> response) {
                //On pourrait vérifier que tout c'est bien passé, mais il n'y a pas de raison que non
                ConnexionInfo.setDepartEnCours(EtatDepartActivity.this, 0);
                Intent intent = new Intent(EtatDepartActivity.this, DepartsActivity.class);
                intent.putExtra(DepartsActivity.IDSOIREE, etat.details.getIdSoiree());
                EtatDepartActivity.this.finish();
            }

            @Override
            public void onFailure(Call<ConnexionReponse> call, Throwable t) {
                Log.e("Desinscription", "Echec de la desinscription");
            }
        });
    }
    public void onModifierClick(View v){
        Intent intent = new Intent(this, NouveauDepartActivity.class);
        intent.putExtra(NouveauDepartActivity.DEPART, etat.details);
        startActivity(intent);
    }
    private void seDesinscrire(){
        APIInterface mApiService = APIGetter.getAPIservice();
        Call<ConnexionReponse> mService = mApiService.desinscrire();
        mService.enqueue(new Callback<ConnexionReponse>() {

            @Override
            public void onResponse(Call<ConnexionReponse> call, retrofit2.Response<ConnexionReponse> response) {
                //On pourrait vérifier que tout c'est bien passé, mais il n'y a pas de raison que non
                ConnexionInfo.setDepartEnCours(EtatDepartActivity.this, 0);
                EtatDepartActivity.this.finish();
            }

            @Override
            public void onFailure(Call<ConnexionReponse> call, Throwable t) {
                Log.e("Desinscription", "Echec de la desinscription");
            }
        });
    }
    public void appelerAuteur(View v){
        etat.details.envoyerSMSAuteur(this);
    }

    public void etendreParticipants(View v){
        //étend ou réduit la liste des participants
        //ici, v est la view aui a appelé cette fonction, donc ici  id/etat_expand
        //on fait faire un demi-tour au bouton, parce que ça fait joli
        ObjectAnimator anim = ObjectAnimator.ofFloat(expandButton, "rotation",rotationAngle, rotationAngle + 180);
        anim.setDuration(500);
        anim.start();
        rotationAngle += 180;
        rotationAngle = rotationAngle%360;

        if(rotationAngle == 180){
            listeParticipants.setVisibility(View.VISIBLE);
            listeParticipants.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;
            listeParticipants.measure(-1,-1);
            DropDownAnim a = new DropDownAnim(listeParticipants, listeParticipants.getMeasuredHeight(), true);
            listeParticipants.getLayoutParams().height = 1;
            a.setDuration(500);
            listeParticipants.startAnimation(a);
        }else{
            DropDownAnim a = new DropDownAnim(listeParticipants, listeParticipants.getHeight(), false);
            a.setDuration(500);
            listeParticipants.startAnimation(a);
        }

    }
    public void envoyerAmi(final String ami){
        APIInterface mApiService = APIGetter.getAPIservice();
        Call<ConnexionReponse> mService = mApiService.inscrireAmi(etat.details.getId(), ami, FirebaseInstanceId.getInstance().getToken());
        mService.enqueue(new Callback<ConnexionReponse>() {

            @Override
            public void onResponse(Call<ConnexionReponse> call, Response<ConnexionReponse> response) {
                if (!response.isSuccessful()) {
                    Log.e("Connexion", response.message());
                    Snackbar.make(participantsTitre, "Erreur: réponse du serveur : " + response.code(), Snackbar.LENGTH_LONG).show();
                    return;
                }
                ConnexionReponse rep = response.body();


                if (rep.getMessage().equals("Valide")){
                    new AlertDialog.Builder(EtatDepartActivity.this)
                            .setTitle(R.string.dialogue_inscription_titre)
                            .setMessage(getString(R.string.dialogue_inscription_message_ami, ami))
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setPositiveButton(R.string.ok, null)
                            .show();

                }else if(rep.getMessage().equals("places")){//message du serveur disant qu'il n'y a plus de places, si quelqu'un s'est inscrip juste avant
                    Snackbar.make(participantsTitre, R.string.plus_de_places, Snackbar.LENGTH_LONG).show();
                    getInformations();
                }
            }

            @Override
            public void onFailure(Call<ConnexionReponse> call, Throwable t) {
                call.cancel();
                Log.e("Connexion", "Erreur fatale de connexion");
                Snackbar.make(participantsTitre, R.string.msg_internet_erreur, Snackbar.LENGTH_LONG).show();
            }
        });

    }
    public void ajouterAmi(View v){
        new MaterialDialog.Builder(this)
                .title(R.string.etat_ami)
                .content(R.string.etat_ami_dialog_msg)
                .inputRangeRes(3, 20, R.color.material_red_500)
                .input(null, null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        envoyerAmi(input.toString());
                    }
                }).show();

    }
    public void envoyerMessage(String message) {
        APIInterface mApiService = APIGetter.getAPIservice();
        Call<ConnexionReponse> mService = mApiService.envoyerMessage(message);
        mService.enqueue(new Callback<ConnexionReponse>() {

            @Override
            public void onResponse(Call<ConnexionReponse> call, Response<ConnexionReponse> response) {
                if (!response.isSuccessful()) {
                    Log.e("Connexion", response.message());
                    Snackbar.make(participantsTitre, "Erreur: réponse du serveur : " + response.code(), Snackbar.LENGTH_LONG).show();
                    return;
                }
                ConnexionReponse rep = response.body();


                if (rep.getMessage().equals("Valide")) {
                    new AlertDialog.Builder(EtatDepartActivity.this)
                            .setTitle(R.string.dialogue_inscription_titre)
                            .setMessage(R.string.message_envoye)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setPositiveButton(R.string.ok, null)
                            .show();

                } else {//message du serveur disant qu'il y a un problème
                    Snackbar.make(participantsTitre, R.string.message_non_envoye, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ConnexionReponse> call, Throwable t) {
                call.cancel();
                Log.e("Connexion", "Erreur fatale de connexion");
                Snackbar.make(participantsTitre, R.string.msg_internet_erreur, Snackbar.LENGTH_LONG).show();
            }
        });

    }
    public void dialogEnvoyerMessage(View v){
        new MaterialDialog.Builder(this)
                .title(R.string.envoyer_message)
                .content(R.string.envoyer_message_txt)
                .inputRangeRes(3, 300, R.color.material_red_500)
                .input(null, getString(R.string.message_depart, ConnexionInfo.getHeureFormat().format(etat.details.getHeure())), new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        envoyerMessage(input.toString());
                    }
                }).show();

    }


}
