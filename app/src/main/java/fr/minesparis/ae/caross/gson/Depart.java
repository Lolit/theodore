package fr.minesparis.ae.caross.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import fr.minesparis.ae.caross.connexion.ConnexionInfo;

/**
 * Created by Louis MEYRAT on 26/12/2016.
 *
 * Généré avec http://www.jsonschema2pojo.org/
 */

public class Depart {


    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("lat")
    @Expose
    private float lat;
    @SerializedName("lon")
    @Expose
    private float lon;
    @SerializedName("adresse")
    @Expose
    private String adresse;
    @SerializedName("transport")
    @Expose
    private int transport;
    @SerializedName("heure")
    @Expose
    private Date heure;
    @SerializedName("distance")
    @Expose
    private float distance;
    @SerializedName("places_totales")
    @Expose
    private int placesTotales;
    @SerializedName("places_utilisees")
    @Expose
    private int placesUtilisees;

    /**
     * No args constructor for use in serialization
     */
    public Depart() {
    }

    public int getId() {
        return id;
    }

    public float getLat() {
        return lat;
    }

    public float getLon() {
        return lon;
    }

    public String getAdresse() {
        return adresse;
    }

    public int getTransport() {
        return transport;
    }

    public String getHeure() {
        return ConnexionInfo.getHeureFormat().format(heure);
    }

    public float getDistance() {
        return distance;
    }

    public int getPlacesTotales() {
        return placesTotales;
    }

    public int getPlacesUtilisees() {
        return placesUtilisees;
    }

    public Depart(int id, float lat, float lon, String adresse, int transport, Date heure, float distance, int placesTotales, int placesUtilisees) {
        super();
        this.id = id;
        this.lat = lat;
        this.lon = lon;
        this.adresse = adresse;
        this.transport = transport;
        this.heure = heure;
        this.distance = distance;
        this.placesTotales = placesTotales;
        this.placesUtilisees = placesUtilisees;
    }
}