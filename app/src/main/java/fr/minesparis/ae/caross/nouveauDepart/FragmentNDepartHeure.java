package fr.minesparis.ae.caross.nouveauDepart;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import fr.minesparis.ae.caross.R;

/**
 * Created by Louis MEYRAT on 25/01/2017.
 *
 * Fragment qui demande la date du départ à l'utilisateur
 */

public class FragmentNDepartHeure extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_ndepart_heure, container, false);
        Button suivant = (Button)v.findViewById(R.id.fragment_ndepart_date_suivant);

        final SimpleDateFormat df = new SimpleDateFormat("dd/MM", Locale.FRANCE);

        final CheckBox demain = (CheckBox) v.findViewById(R.id.ndepart_heure_demain);
        final TextView dateView = (TextView) v.findViewById(R.id.ndepart_date);
        demain.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, b?1:0);
                dateView.setText(getActivity().getString(R.string.ndepart_date, df.format(cal.getTime())));
            }
        });
        demain.setChecked(false);//on change l'état pour déclancher le listener une première fois

        final TimePicker heure = (TimePicker) v.findViewById(R.id.ndepart_heure);
        NouveauDepartActivity act = ((NouveauDepartActivity)getActivity());
        if(!act.isNouveau()){
            Calendar cal = Calendar.getInstance();
            cal.setTime(act.getDepart().getHeure());
            heure.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
            demain.setChecked(cal.get(Calendar.DAY_OF_YEAR)!=Calendar.getInstance().get(Calendar.DAY_OF_YEAR));
        }else{
            heure.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
        }

        suivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, demain.isChecked()?1:0);
                cal.set(Calendar.HOUR_OF_DAY, heure.getCurrentHour()); //on ne peut utiliser que la méthode dépréciée, car la nouvelle n'est pas rétrocompatible
                cal.set(Calendar.MINUTE, heure.getCurrentMinute());

                if(cal.getTimeInMillis()<Calendar.getInstance().getTimeInMillis()+600000){
                    Snackbar.make(dateView, R.string.ndaper_heure_erreur, Snackbar.LENGTH_SHORT).show();
                    return;
                }

                ((NouveauDepartActivity)getActivity()).setHeure(cal);
                FragmentNDepartDestination frag = new FragmentNDepartDestination();
                ((NouveauDepartActivity)getActivity()).showFragment(frag, true);
            }
        });
        return v;

    }




}
