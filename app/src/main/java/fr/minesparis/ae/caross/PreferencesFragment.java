package fr.minesparis.ae.caross;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

/**
 * Created by Louis MEYRAT on 27/12/2016.
 *
 */

public class PreferencesFragment extends PreferenceFragment {

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.parametres);

        if(BuildConfig.DEBUG) {
            Preference button = findPreference(getString(R.string.pref_reset));
            button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(PreferencesFragment.this.getActivity());
                    prefs.edit().clear().apply();
                    return false;
                }
            });
        }

        Preference button = findPreference(getString(R.string.pref_about));
        button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                startActivity(new Intent(PreferencesFragment.this.getActivity(), AboutActivity.class));
                return false;
            }
        });
    }

}
