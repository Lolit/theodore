package fr.minesparis.ae.caross.views;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.util.Calendar;

import fr.minesparis.ae.caross.DepartsActivity;
import fr.minesparis.ae.caross.R;
import fr.minesparis.ae.caross.connexion.ConnexionInfo;
import fr.minesparis.ae.caross.gson.Soiree;

/**
 * Created by Louis MEYRAT on 14/03/2017.
 *
 * Un élément de la liste des soirées utilisé dans le SoireesAdapter
 */

class SoireeHolder extends RecyclerView.ViewHolder{

    private TextView nom;
    private TextView text;
    private ImageView image;
    private TextView codePostal;
    private TextView ville;
    WebView details;
    ImageView expandButton;

    SoireeHolder(View itemView) {
        super(itemView);

        nom = (TextView) itemView.findViewById(R.id.nomSoiree);
        text = (TextView) itemView.findViewById(R.id.auteurSoiree);
        image = (ImageView) itemView.findViewById(R.id.photoSoiree);
        codePostal = (TextView)itemView.findViewById(R.id.codePostal);
        ville = (TextView)itemView.findViewById(R.id.villeSoiree);
        details = (WebView)itemView.findViewById(R.id.soiree_details);
        expandButton = (ImageView) itemView.findViewById(R.id.boutonDetailsSoiree);

        details.setBackgroundColor(0x00000000);
        details.setHorizontalScrollBarEnabled(false);
        details.getSettings().setJavaScriptEnabled(true);
        details.getSettings().setDomStorageEnabled(true);
        details.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        details.setDrawingCacheEnabled(false);
    }

    /**
     * Permet de modifier les view de ce ViewHolder en fonction de la Soiree passé en paramètres
     * @param soiree
     * La soirée qui contient les données sue l'on veut afficher dans ce ViewHolder
     */
    void bind(final Soiree soiree){
        //il ne reste plus qu'à remplir notre vue
        nom.setText(soiree.getNom());
        text.setText(text.getContext().getString(R.string.departs_par)+soiree.getAuteur());
        Ion.with(image)
                //.placeholder(ImageView.)
                .error(R.drawable.ic_image_error)
                //.animateLoad(spinAnimation)
                //.animateIn(fadeInAnimation)Si on veut ajouter une animation
                .load(ConnexionInfo.BASE_URL+"../image/"+soiree.getUrlImage());
        codePostal.setText(soiree.getCodePostal());
        ville.setText(soiree.getVille());

        itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(text.getContext(), DepartsActivity.class);
                intent.putExtra(DepartsActivity.IDSOIREE, soiree.getId());
                text.getContext().startActivity(intent);
            }
        });
        details.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Calendar cal = Calendar.getInstance();
                cal.setTime(soiree.getHeureDebut());
                view.loadUrl("javascript:setClock("+cal.get(Calendar.HOUR_OF_DAY)+", "+cal.get(Calendar.MINUTE)+")");
                view.loadUrl("javascript:replace('date', '"+ ConnexionInfo.getDateFormat().format(soiree.getHeureDebut())+"')");
            }
        });

        details.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        Log.d("details", ConnexionInfo.BASE_URL + "../details_soiree/" + soiree.detailsURL + details.isHardwareAccelerated());
        details.loadUrl(ConnexionInfo.BASE_URL + "../details_soiree/" + soiree.detailsURL);


    }
}
