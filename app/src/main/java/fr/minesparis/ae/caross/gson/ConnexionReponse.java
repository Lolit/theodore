package fr.minesparis.ae.caross.gson;

/**
 * Created by Louis MEYRAT on 15/12/2016.
 *
 * permet de déserialiser la réponse à une tentative de connexion
 */

public class ConnexionReponse {

    private String valide;
    public static final String VALIDE = "Valide";

    public ConnexionReponse(String valide) {
        this.valide = valide;
    }

    public String getMessage() {
        return valide;
    }
}
