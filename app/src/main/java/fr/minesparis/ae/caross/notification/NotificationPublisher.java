package fr.minesparis.ae.caross.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


/**
 * Classe trouvée sur https://gist.github.com/BrandonSmith/6679223
 * a été un peu modifiée pour faire ce que je veux
 *
 * Permet d'émettre une notification qui est passée dans un intent, utilisée pour envoyer des notifications à des heures précises
 */
public class NotificationPublisher extends BroadcastReceiver {

    public static final int DEPART_EN_COURS = 23;
    public static final int DEPART_IMMINENT = 24;

    public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION = "notification";

    public void onReceive(Context context, Intent intent) {

        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        Log.d("Alarme", "Dring Dring !");
        Notification notification = intent.getParcelableExtra(NOTIFICATION);
        int id = intent.getIntExtra(NOTIFICATION_ID, 0);
        notificationManager.notify(id, notification);

        notificationManager.cancel(DEPART_EN_COURS);

    }
}